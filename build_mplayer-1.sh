#!/bin/sh
#
# build and installer mplayer
# by claudio borges but3k4 --> but3k4@linuxmail.org
# version: 0.2
#
declared_vars () 
{
version=1.0pre4
arch=i486
build=1but
tar=`which tar`
wget=`which wget`
mplayer_source="MPlayer-$version.tar.bz2"
mplayer_dir="MPlayer-$version"
codecs_source="essential.tar.bz2"
codecs_dir="extralite"
win32codecs_source="win32codecs-lite.tar.bz2"
win32codecs_dir="win32codecs-lite"
skins_source="proton-1.2.tar.bz2"
skins_dir="proton"
fonts_source="font-arial-iso-8859-1.tar.bz2"
fonts_dir="font-arial-iso-8859-1"
url_mplayer="http://www1.mplayerhq.hu/MPlayer/releases/$mplayer_source"
url_codecs="http://www1.mplayerhq.hu/MPlayer/releases/codecs/$codecs_source"
url_win32codecs="http://www1.mplayerhq.hu/MPlayer/releases/codecs/$win32codecs_source"
url_skins="http://www1.mplayerhq.hu/MPlayer/Skin/$skins_source"
url_fonts="http://www1.mplayerhq.hu/MPlayer/releases/fonts/$fonts_source"
}

create_dirs ()
{
if [ "$TMP" = "" ]; then
        TMP=/tmp
fi

if [ "$BUILD_DIR" = "" ]; then
	BUILD_DIR=$TMP/pkg-mplayer
fi

if [ ! -d $TMP ]; then
        mkdir -p $TMP
fi

if [ ! -d $BUILD_DIR ]; then
	mkdir -p $BUILD_DIR
fi
}

download ()
{
cd $TMP
$wget $url_mplayer
$wget $url_codecs
$wget $url_win32codecs
$wget $url_skins
$wget $url_fonts
}

compile () {
cd $TMP
$tar xvjf $mplayer_source
cd $mplayer_dir

CFLAGS="-O2 -march=i486 -mcpu=i686" \
CXXFLAGS="-O2 -march=i486 -mcpu=i686" \
./configure --prefix=/usr \
	--confdir=/etc/mplayer \
	--enable-gui 
make
make install DESTDIR=$BUILD_DIR
mkdir -p $BUILD_DIR/usr/doc/mplayer-$version
cp -a AUTHORS ChangeLog Copyright LICENSE README \
DOCS/HTML/en $BUILD_DIR/usr/doc/mplayer-$version
cp -a etc/codecs.conf etc/input.conf $BUILD_DIR/etc/mplayer/
}

skins_fonts_codecs ()
{
cd $TMP
tar xvjf $codecs_source
mv $codecs_dir $BUILD_DIR/usr/lib/win32
tar xvzf $win32codecs_source
mv $win32codecs_dir/* $BUILD_DIR/usr/lib/win32/
tar xvjf $skins_source
mv $skins_dir $BUILD_DIR/usr/share/mplayer/Skin/default
tar xvjf $fonts_source
cd $fonts_dir/font-arial-18-iso-8859-1
cp -a * $BUILD_DIR/usr/share/mplayer/font/
chown -R root:root $BUILD_DIR
}

create_scripts ()
{
mkdir -p $BUILD_DIR/install
cat > $BUILD_DIR/install/slack-desc << SLACKDESK
      |-----handy-ruler------------------------------------------------------|
mplayer: MPlayer (movie player)
mplayer:
mplayer: MPlayer is a movie player for LINUX. It plays most MPEG, VOB, AVI,
mplayer: OGG, VIVO, ASF/WMV, QT/MOV, FLI, RM, NuppelVideo, yuv4mpeg, FILM,
mplayer: RoQ files, supported by many native, XAnim, and Win32 DLL codecs.
mplayer: You  can  watch VideoCD, SVCD, DVD, 3ivx, DivX 3/4/5 and even WMV
mplayer: movies too. MPlayer supports 10 types of subtitles formats: VobSub,
mplayer: MicroDVD, SubRip, SubViewer, Sami, VPlayer, RT, SSA, AQTitle, MPsub.
mplayer: 
mplayer:
mplayer:
SLACKDESK
}
create_package ()
{
cd $BUILD_DIR
makepkg -l y -c n mplayer-$version-$arch-$build.tgz
}

install_mplayer ()
{
echo -en "\nDo you liked of install mplayer? Y/n: "
read result

if [ "$result" = "y" -o "$result" = "Y" -o -z "$result" ]; then
	if [ -e $BUILD_DIR/mplayer-$version-$arch-$build.tgz ]; then
	cd $BUILD_DIR
	installpkg mplayer-$version-$arch-$build.tgz
	fi
else
	echo -e "\nThe mplayer is not installed\n"
fi
}

declared_vars
create_dirs
download
compile
skins_fonts_codecs
create_scripts
create_package
install_mplayer
