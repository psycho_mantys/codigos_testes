#!/bin/bash


if [[ "x$1" == 'x' ]] ; then 
	echo 'informe interface!';
	exit 1;
fi

#se ip for fixo, experimente ethX onde X = numero

iptables -t nat -A PREROUTING -i ${1} -p tcp --dport 6881:6889 -j DNAT --to-dest 192.168.1.35
iptables -A FORWARD -p tcp -i ${1} --dport 6881:6889 -d 192.168.1.35 -j ACCEPT
iptables -t nat -A PREROUTING -i ${1} -p udp --dport 6881:6889 -j DNAT --to-dest 192.168.1.35
iptables -A FORWARD -p udp -i ${1} --dport 6881:6889 -d 192.168.1.35 -j ACCEPT
