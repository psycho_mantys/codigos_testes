#!/bin/sh
#
# compile and install squidguard proxy redirectonr
# by groo --> microbiu@terra.com.br
# based on similar tools from claudio borges --> claudio@onerd.com.br
# version: 0.2
#


declared_vars () 
{
version=1.2.0p3
arch=i486
build=1groo
tar=`which tar`
wget=`which wget`
squidguard_source="squidGuard-$version.tar.gz"
squidguard_dir="squidGuard-$version"
url="http://squidguard.shalla.de/Downloads/$squidguard_source"
}

create_dirs ()
{
if [ "$TMP" = "" ]; then
        TMP=/tmp
fi

if [ "$BUILD_DIR" = "" ]; then
	BUILD_DIR=$TMP/pkg-squidguard
fi

if [ ! -d $TMP ]; then
        mkdir -p $TMP
fi

if [ -d $BUILD_DIR ]; then
	rm -rf $BUILD_DIR
	mkdir -p $BUILD_DIR
else
	mkdir -p $BUILD_DIR
fi
}

download ()
{
cd $TMP
if [ ! -e $squidguard_source ]; then
	$wget -c $url
fi
}

compile () {
cd $TMP
if [ ! -d $squidguard_dir ]; then
	rm -rf $squidguard_dir
fi

if [ -r $squidguard_source ]; then
	$tar xzvf $squidguard_source
else
	echo "No source tarball available !"
	exit 1
fi

cd $squidguard_dir

CFLAGS="-O2 -march=i486 -mcpu=i686" \
CXXFLAGS="-O2 -march=i486 -mcpu=i686" \
./configure --prefix=/usr/squidGuard \
		--sbindir=/usr/sbin \
		--with-sg-config=/usr/squidGuard/squidGuard.conf \
		--with-sg-logdir=/usr/squidGuard/log
cat Makefile | sed -e 's/squidguard/squidGuard/g' > /tmp/Makefile
cat /tmp/Makefile > Makefile
make
if [ ! $? = 0 ]; then
	echo -e "\n\nAn error ocurred during compilation process. Please solve the problems and try again\n\n"
	exit
	return 0
fi
make install DESTDIR=$BUILD_DIR
mv /usr/squidGuard $BUILD_DIR/usr
rm $BUILD_DIR/usr/squidGuard/squidGuard.conf
cp -ra doc contrib samples test $BUILD_DIR/usr/squidGuard
chown -R root:root $BUILD_DIR
chown -R squid:squid $BUILD_DIR/usr/squidGuard
}

create_scripts ()
{
mkdir -p $BUILD_DIR/install
cat > $BUILD_DIR/install/slack-desc << SLACKDESK
      |-----handy-ruler------------------------------------------------------|
squid: squidguard (Proxy Server URL Redirector)
squid:
squid: SquidGuard is a URL redirector used to use blacklists with the proxy
squid: software Squid. There are two big advantages to squidguard: it is
squid: fast and it is free
squid: 
squid: The original developers are not supporting squidguard anymore.
squid:
squid: The people from http://squidguard.shalla.de/ has some adds to original
squid: applications which some new improvements.
squid:
SLACKDESK
mkdir -p $BUILD_DIR/usr/local/sbin
cat > $BUILD_DIR/usr/local/sbin/blacklist.sh << BLACKLIST
#!/bin/sh
#
# SquidGuard Shalla.de Blacklist Automatic Atualizer
# 
# by groo --> microbiu@terra.com.br
#
# usage >> sh /usr/local/sbin/blacklist.sh (0|1)

WGET=\`which wget\`
MD5SUM=\`which md5sum\`
CAT=\`which cat\`
SQUIDGUARD=\`which squidGuard\`
SHALLALIST="http://squidguard.shalla.de/Downloads/shallalist.tar.gz"

CAMINHO="/usr/squidGuard/db/"
cd \$CAMINHO

if [ ! $1 ==  0 ];then
	        \$WGET -c \$SHALLALIST
fi
	

if [ \$? = 0 ];then

MD5NEW="\$(\$MD5SUM shallalist.tar.gz | cut -d " " -f1 )"
MD5OLD="\$(\$CAT md5.txt)"

        if [ \$MD5NEW = \$MD5OLD ];then
                rm shallalist.tar.gz
        else
                for i in `find ./ -type f -name "*.db"`; do
                        rm -v \$i
                done
                if [ ! \$MD5NEW="" ]; then
                        echo \$MD5NEW > md5.txt
                fi
                tar -zxvf shallalist.tar.gz
                mv shallalist.tar.gz shallalist.tar.gz.old
                \$SQUIDGUARD -C all
                chown squid.squid * -R
        fi
fi
BLACKLIST
chmod +x $BUILD_DIR/usr/local/sbin/blacklist.sh
cat > $BUILD_DIR/usr/squidGuard/squidGuard.conf << CONFIGFILE
#
# CONFIG FILE FOR SQUIDGUARD
#

dbhome /usr/squidGuard/db/BL
logdir /usr/squidGuard/log


dest porn {
        domainlist porn/domains
        urllist porn/urls
        }

dest movies  {
        domainlist movies/domains
        urllist movies/urls
        }

dest webradio {
        domainlist webradio/domains
        urllist webradio/urls
        }

dest chat {
        domainlist chat/domains
        urllist chat/urls
        }

dest white {
        domainlist white/domains
        }

dest redirector {
        domainlist redirector/domains
        urllist redirector/urls
        }

dest games {
        domainlist hobby/games/domains
        urllist hobby/games/urls
        }

dest dating {
        domainlist dating/domains
        }

acl {
        default {
        pass white !in-addr !porn !webradio !movies !games !chat !redirector !dating all
        redirect http://matriz.vianet-express.com.br/oops.html
        }
}
CONFIGFILE
mkdir -p $BUILD_DIR/usr/squidGuard/db/BL/white
echo "putyourwhitelisthere" > $BUILD_DIR/usr/squidGuard/db/BL/white/domains
} 

create_package ()
{
cd $BUILD_DIR
makepkg -l y -c n squidguard-$version-$arch-$build.tgz
}

install_squidguard ()
{
echo -en "\nWould you like to install squidguard? Y/n: "
read result

if [ "$result" = "y" -o "$result" = "Y" -o -z "$result" ]; then
	if [ -e $BUILD_DIR/squidguard-$version-$arch-$build.tgz ]; then
	cd $BUILD_DIR
	installpkg squidguard-$version-$arch-$build.tgz
	fi

	echo -en "\nDo you wanna download and build shalla's blacklists databases?\n(It may takes a few minutes and download about 2MB) Y/n: "
	read result
	if [ "$result" = "y" -o "$result" = "Y" -o -z "$result" ]; then
		sh /usr/local/sbin/blacklist.sh
		ps ax | grep -q crond
		if [ $? = 0 ];then
			echo -en "\n Do you wanna add weekly automatic blacklists atualization to your crontab? Y/n"
			read result
			if [ "$result" = "y" -o "$result" = "Y" -o -z "$result" ]; then
				echo -en "#\n# Run weekly blacklists update at 4:35 of first day of week:\n \
35 4 \* \* 0 /usr/bin/run-parts /etc/cron.weekly 1> /dev/null" >> /var/spool/cron/crontabs/root
			fi
		fi
		echo -en "\n Now you just need to add the following line to your squid.conf:\n\n \
redirect_program /usr/bin/squidGuard -c /usr/squidGuard/squidGuard.conf\n"
		echo -en "\nEnjoy it!\n"
	fi
else
       	echo -e "\nThe squid is not installed\n"
fi
}

declared_vars
create_dirs
download
compile
create_scripts
create_package
install_squidguard
