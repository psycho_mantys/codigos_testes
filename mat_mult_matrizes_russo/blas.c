#include <mkl.h>

// A, B and C are all (NxN) matrices

extern "C"
{
void Multiply ( double *A, double *B, double *C, int *M )
{	
	int N = *M;
	cblas_dgemm ( CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0, A, N, B, N, 0.0, C, N );
}
}
