#ifndef MY_STACK_PSYCHO
#define MY_STACK_PSYCHO

#include <stdio.h>
#include <stdlib.h>
#include "node.cpp"

template <typename data>
class stack {
	typedef node<data> Node;

	Node *begin;
	unsigned int size;
	public:
		stack() : begin(NULL), size(0)
		{
		}

		inline bool is_empty(){
			return begin==NULL;
		}

		inline data pop(){
			if( begin ){
				data ret = begin->data();
				Node *n=begin->next();
				delete begin;
				begin=n;
				--size;
				return ret;
			}else{
				fprintf(stderr, "Chuck Norris dislikes pilha\n");
				exit(-1);
			}
		}

		inline void push( const data &in ){
			Node *n( new Node(in,begin) );
			begin = n ;
			++size;
		}

		~stack(){
			while( ! this->is_empty() ){
				this->pop();
			}
		}
};

#endif // ifdef MY_STACK_PSYCHO

