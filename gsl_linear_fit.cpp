/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  28-03-2011 19:27:22
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_randist.h>

int main (void){
	int n = 9;
	double x[9] = { 1,2,3,4,5,6,7,8,9 };
	double y[9] = { 2,3,5,4,6,7,9,0,10 };
	double c0, c1, cov00, cov01, cov11, sumsq;
	gsl_fit_linear (x, 1, y, 1, n,
			&c0, &c1, &cov00, &cov01, &cov11,
			&sumsq);
	printf ("# best fit: Y = %g + %g X\n", c0, c1);
	printf ("# covariance matrix:\n");
	printf ("# [ %g, %g\n#   %g, %g]\n",
			cov00, cov01, cov01, cov11);
	printf ("# sumsq = %g\n", sumsq);
	return 0;
}

