#!/bin/bash

VERSION=4.3.3

mkdir "/gcc-${VERSION}"

wget -c "ftp://ftp.unicamp.br/pub/gnu/gcc/gcc-${VERSION}/gcc-${VERSION}.tar.bz2"

tar -xjvf gcc-"${VERSION}".tar.bz2  || ( echo "erro ao extrair. Verifique se o pacote foi baixado corrtament.

" ; exit 1 )

cd "gcc-${VERSION}"

../gcc-"${VERSION}"/configure --prefix=/gcc-"${VERSION}"/ --libexecdir=/gcc-"${VERSION}"/lib --with-local-prefix=/gcc-"${VERSION}" --enable-threads=posix  --enable-__cxa_atexit --disable-checking --enable-shared --enable-languages=c,c++

make BOOT_LDFLAGS="-static" bootstrap

make install

echo "/gcc-${VERSION}/lib/" >> /etc/ld.so.conf

ldconfig

ln -s /gcc-${VERSION}/bin/gcc /usr/bin/gcc-"${VERSION}"
ln -s /gcc-${VERSION}/bin/g++ /usr/bin/g++-"${VERSION}"
 

