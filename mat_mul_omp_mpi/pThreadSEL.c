/*****************************************************************************
 * FILE: pThreadSEL.c
 ******************************************************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "mpi.h"

#define ASCII 


typedef struct 
{
	double	**A;
	double	*b;
	double	*x0;
	double	*x;
	int	n;
	int	nitemax;
	int	nite;
	double	tol;
	double	norma;
	double	norma2;
} SELDATA;

typedef struct
{
	int nlpp;
	int start;
	int id;

} PROCDATA;

struct time{
	
	double t_aloc;
	double t_leit;
	double t_proc;
	double t_comun;
	double t_total;
} Time;

/* /declaracao das variaveis globais */
SELDATA selstr;
PROCDATA *procstr;
pthread_t *callThd;
pthread_mutex_t mutexsum;


int num;
pthread_barrier_t barrier;
int NUMTHRDS;

/* ///////////////////////////////////////////////////////////////////////////////////////////////// */
/* // Funcao GaussJacobi*/
/* ///////////////////////////////////////////////////////////////////////////////////////////////// */

void *GaussJacobi(void *arg)
{

	int i,j, start, end, nlp ;
	double my_norma,my_norma_sum;
	double **AA, *bb,*xx0;
	double s;
	int nite;


	PROCDATA *my_data;
	my_data = (PROCDATA *) arg;

	nlp = my_data->nlpp;
	start = my_data->start;
	end   = start + nlp;

	AA = selstr.A;
	bb = selstr.b;
	xx0 = selstr.x0;

	selstr.norma2 = 10* (selstr.tol);
	my_norma_sum = 10* (selstr.tol);

	nite = 0;

/*	//while (nite<=(selstr.nitemax) && selstr.norma2 > (selstr.tol)){*/
	while (nite<=(selstr.nitemax)){

/*		// Processamento da parte do sistema correspondente a thread em questao*/
		for(i=start;i<end;i++)
		{
			s=0;

			/* 2 for para que seja excluido o caso de i==j, sem que exista um if( i==j ) a cada iteracao */
			for(j=0 ; j<=i ; ++j){
				s+=AA[i][j]*xx0[j];
			}
			++j;
			for(; j<(selstr.n) ; ++j){
				s+=AA[i][j]*xx0[j];
			}

			selstr.x[i] = (bb[i]-s)/AA[i][i];
		}


		my_norma=0;

		for (i=start;i<end;i++){
			my_norma=my_norma+ (selstr.x[i]-xx0[i])*(selstr.x[i]-xx0[i]);
			xx0[i]= selstr.x[i];
		}

		pthread_mutex_lock (&mutexsum);
		selstr.norma += my_norma;
		pthread_mutex_unlock (&mutexsum);


		pthread_barrier_wait(&barrier);
		selstr.norma = sqrt(selstr.norma);
		pthread_barrier_wait(&barrier);
		selstr.norma=0;	

		nite++;

	}

	selstr.nite = nite;

	pthread_exit((void*) 0);
}

/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Fim da Funcao GaussJacobi
///////////////////////////////////////////////////////////////////////////////////////////////// */


/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Funcao ReadDataFile
///////////////////////////////////////////////////////////////////////////////////////////////// */
int readDataFile(FILE *fp, double ***A, double **b, double **x0, double **x, int *nmaxite, double *tol) {

	int n =3, i, j;

	/*Pega o tamanho da matriz*/
	fscanf(fp,"%d",&n);

/*	//inicia a contagem do tempo de alocação */
	Time.t_aloc = MPI_Wtime() ;

	/*Aloca a matriz e os vetores com base no n*/
	*A = calloc(n, sizeof(double *));
	for (i = 0; i < n; i++)
		(*A)[i] = calloc(n, sizeof(double));
	*b  = calloc(n, sizeof(double));
	*x0 = calloc(n, sizeof(double));
	*x = calloc(n, sizeof(double));

/*	//finaliza a contagem do tempo de alocação*/
	Time.t_aloc = MPI_Wtime() - Time.t_aloc ;

/*	//inicia a contagem do tempo de leitura do arquivo de entrada */
	Time.t_leit = MPI_Wtime() ;

	/*Atribui os valores lidos do arquivo à matriz e aos vetores*/
	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			fscanf(fp, "%lf", &((*A)[i][j]));
	for(i = 0; i < n; i++)
		fscanf(fp, "%lf", &((*b)[i]));
	for(i = 0; i < n; i++)
		fscanf(fp, "%lf", &((*x0)[i]));
	fscanf(fp, "%d", nmaxite);
	fscanf(fp, "%lf", tol);

/*	//finaliza a contagem do tempo de alocação*/
	Time.t_leit = MPI_Wtime() - Time.t_leit ;

	return n;
}

/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Fim da Funcao ReadDataFile
/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////// 
// Funcao main
///////////////////////////////////////////////////////////////////////////////////////////////// */
int main (int argc, char *argv[])
{
/*	//Declaracao das variaveis */
	int    i;
	int    n,nlp; 
	double resto;
	int    start;
	FILE   * fpin;
	double **A;
	double *b,*x0;
	double tol;
	int    nmaxite;
	int    status;
	pthread_attr_t attr;

/*	//Número de threads*/
	NUMTHRDS = atoi(argv[2]);

	printf("Numero de threads = %d\n",NUMTHRDS);

/*	//Inicializacao do MPI apenas pra utilizar a funcao que mede tempo*/
	MPI_Init(&argc,&argv);

/*	//inicia a contagem do tempo total de execução*/
	Time.t_total = MPI_Wtime();

/*	// Abre arquivo de entrada */
	fpin = fopen(argv[1],"r");

	if (fpin==NULL){
		fprintf(stdout,"Erro ao tentar abrir o arquivo de dados\n");
		fflush(stdout);
	}

/*	// Alocacao dinamica e leitura dos dados de entrada */
	n = readDataFile(fpin, &A, &b, &x0, &selstr.x, &nmaxite, &tol);

/*	//Alocacao dinamica do vetor de threads */
	callThd = (pthread_t *) malloc (NUMTHRDS*sizeof(pthread_t));

/*	//Alocacao dinamica do vetor de estrutura de dados */
	procstr = (PROCDATA *) malloc (NUMTHRDS*sizeof(PROCDATA)); 

/*	// Calcula numero de linhas por thread */
	nlp   = (int)n/NUMTHRDS;
	for(i=0;i<NUMTHRDS;i++)
		procstr[i].nlpp = nlp;

/*	// Calcula resto da divisao */
	resto = (int)n%NUMTHRDS;
/*	// Incrementa mais um processo para cada processador ate o resto ser igual a zero*/
	i = 0;
	if (resto!=0){
		while (resto>0){
			(procstr[i].nlpp)++;
			resto--;
			i++;
		}
	}

/*	//atribui os dados da estrutura */
	selstr.A = A; 
	selstr.b = b;
	selstr.x0 = x0;
	selstr.nitemax = nmaxite;
	selstr.tol = tol;
	selstr.n = n;

/*	//Inicializacao da MUTEX*/
	pthread_mutex_init(&mutexsum, NULL);

/*	//inicializacao da barreira*/
	pthread_barrier_init(&barrier, NULL, NUMTHRDS);

/*	//criação dos atributos das threads*/
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

/*	//inicializacao da variavel norma*/
	selstr.norma = 0; 

/*	//inicia a contagem do tempo de processamento*/
	Time.t_proc = MPI_Wtime() ;


/*	//criação das threads*/
	start = 0;
	for(i=0;i<NUMTHRDS;i++)
	{

		procstr[i].start = start;
		procstr[i].id = i;

		pthread_create( &callThd[i], &attr , GaussJacobi, (void *)&procstr[i]); 

		start +=(procstr[i].nlpp);
	}

/*	//finaliza a contagem do tempo de processamento*/
	Time.t_proc = MPI_Wtime() - Time.t_proc ;

	pthread_attr_destroy(&attr);

	/* Wait on the other threads */
	for(i=0;i<NUMTHRDS;i++) {
		pthread_join( callThd[i], (void *)&status);
	}

/*	//destruicao da barreia*/
	pthread_barrier_destroy(&barrier);

/*	// Impressao da solucao do sistema */
	fprintf(stdout,"A solucao do encontrada apos %d iteracoes foi:\n",selstr.nite);
	fprintf(stdout,"X[%d] = %f\n",0,selstr.x[0]);
	fprintf(stdout,"X[%d] = %f\n",n-1,selstr.x[n-1]);


/*	//fechamento dos arquivos*/
	fclose(fpin);

/*	//liberacao da nemoria alocada*/
	for(i=0;i<n;i++)
		if (A[i]!=NULL) free(A[i]);
	if (A!=NULL) free(A);
	if (b!=NULL) free(b);
	if (x0!=NULL) free(x0);
	if (selstr.x!=NULL)free(selstr.x);

	pthread_mutex_destroy(&mutexsum);

/*	//finaliza a contagem do tempo total de execução*/
	Time.t_total = MPI_Wtime() -Time.t_total;

/*	//impressão dos tempos
	//impressão dos tempos*/
	printf(" \n\nFIM DA EXECUCAO : ");
	printf( " \n\nNome do Executavel:%s",argv[0]);
	printf( " \nNumero de Threads:%d",NUMTHRDS);
	printf("\nArquivo de Entrada:%s",argv[1]);
	printf("\n\nTempo de Alocacao:%f",Time.t_aloc);
	printf("\nTempo de Leitura:%f",Time.t_leit);
	printf("\nTempo de Processamento:%f",Time.t_proc);
	printf("\nTempo total:%f\n\n",Time.t_total);	
	
/*	// Finaliza o MPI */
	MPI_Finalize();

	pthread_exit(NULL);

}

