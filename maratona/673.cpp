#include <iostream>
#include "stack.cpp"

using namespace std;

int main(){

	unsigned int qtd;

	scanf("%i%c",&qtd) ;

	for( unsigned int i=0 ; i<qtd ; ++i ){
		string in ;
		stack<char> s;
		unsigned int index;

		getline (cin,in);
//		cin >> in ;

		for( index=0 ; index < in.size() ; ++index ){
			if( '[' == in.at(index) || '(' == in.at(index) ){
				s.push(in.at(index));
			}else{
				if( in.at(index) == ']' ){
					if( s.is_empty() || s.pop() != '[' ){
						break;
					}
				}else{
					if( s.is_empty() || s.pop() != '(' ){
						break;
					}
				}
			}
		}

		if( index==in.size() && s.is_empty() ){
			cout << "Yes" << endl ;
		}else{
			cout << "No"  << endl ;
		}
	}
	return 0;
}

