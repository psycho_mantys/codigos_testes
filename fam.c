/*
 *   monitor.c -- monitor arbitrary file or directory
 *                using fam
*/
 
#include <fam.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/select.h>
 
/* event_name() - return printable name of fam event code */
 
const char *event_name(int code)
{
    static const char *famevent[] = {
        "",
        "FAMChanged",
        "FAMDeleted",
        "FAMStartExecuting",
        "FAMStopExecuting",
        "FAMCreated",
        "FAMMoved",
        "FAMAcknowledge",
        "FAMExists",
        "FAMEndExist"
    };
    static char unknown_event[10];
 
    if (code < FAMChanged || code > FAMEndExist)
    {
        sprintf(unknown_event, "unknown (%d)", code);
        return unknown_event;
    }
    return famevent[code];
}
 
int main(int argc, char *argv[])
{
    int i, nmon, rc, fam_fd;
    FAMConnection fc;
    FAMRequest *frp;
    struct stat status;
    FAMEvent fe;
    fd_set readfds;
 
    /* Allocate storage for requests */
 
    frp = malloc(argc * sizeof *frp);
    if (!frp)
    {
        perror("malloc");
        exit(1);
    }
 
    /* Open fam connection */
 
    if ((FAMOpen(&fc)) < 0) 
    {
        perror("fam");
        exit(1);
    }
 
    /* Request monitoring for each program argument */
 
    for (nmon = 0, i = 1; i < argc; i++)
    {
        if (stat(argv[i], &status) < 0)
        {
            perror(argv[i]);
            status.st_mode = 0;
        }
        if ((status.st_mode & S_IFMT) == S_IFDIR)
            rc = FAMMonitorDirectory(&fc, argv[i], frp + i,
                                     NULL);
        else
            rc = FAMMonitorFile(&fc, argv[i], frp + i, NULL);
        if (rc < 0)
        {
            perror("FAMMonitor failed");
            continue;
        }
        nmon++;
    }
    if (!nmon)
    {
        fprintf(stderr, "Nothing monitored.\n");
        exit(1);
    }
 
    /* Initialize select data structure */
 
    fam_fd = FAMCONNECTION_GETFD(&fc);
    FD_ZERO(&readfds);
    FD_SET(fam_fd, &readfds);
 
    /* Loop forever. */
 
    while(1)
    {
        if (select(fam_fd + 1, &readfds,
                   NULL, NULL, NULL) < 0)
        {
             perror("select failed");
             exit(1);
        }
	printf("fim select\n");
        if (FD_ISSET(fam_fd, &readfds))
        {
            if (FAMNextEvent(&fc, &fe) < 0)
            {
                perror("FAMNextEvent");
                exit(1);
            }
            printf("%-24s %s\n", fe.filename,
                   event_name(fe.code));
        }
	printf("fim while\n");
    }
	return 0;
}

