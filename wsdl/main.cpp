/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  19-08-2009 19:34:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys (P.M.), psycho.mantys(.AT,)gmail dot com
 *        Company:  LCCV
 *
 * =====================================================================================
 */

struct Namespace namespaces[] =
{
  { "SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/" },
  { "SOAP-ENC","http://schemas.xmlsoap.org/soap/encoding/"},
  { "xsi", "http://www.w3.org/1999/XMLSchema-instance" },
  { "xsd", "http://www.w3.org/1999/XMLSchema" },
  { "ns", "http://sura-vims-pe6600-1.vims.edu/Demo" },
  { NULL, NULL }    // end of table
};

#include "soapH.h"
#include "soapmyServiceSoapBindingProxy.h"
#include <iostream>

using namespace std;

int main(){
	char resp[1000] ;
	char *r = new char[1000] ;
	r[0] = '\n' ;
	myServiceSoapBinding x;
	x.soap = soap_new() ;
	x.endpoint = "http://sura-vims-pe6600-1.vims.edu/~drf/cgi-bin/oostech.cgi" ;
	cout << "oioioi" << endl ;
	x.ns1__wsdl(r);
	cout << "wsdl:" << r << endl << endl ;
	soap_print_fault(x.soap, stderr);
	soap_call_ns1__wsdl(x.soap , "http://sura-vims-pe6600-1.vims.edu/~drf/cgi-bin/oostech.cgi" , "" , r);
	cout << "wsdl:" << r << endl << endl ;
	x.ns1__getLatest("salinity",NULL,r);
	cout << "getLatest(\"salinity\",r):" << r << endl << endl ;
	
//	soap_delete( x.soap );
	free(r);
}


