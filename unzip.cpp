/*
 * =====================================================================================
 *
 *       Filename:  unzip.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  26-05-2011 02:25:58
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys (P.M.), psycho.mantys(.AT,)gmail dot com
 *        Company:  LCCV
 *
 * =====================================================================================
 */

#include	<iostream>
#include	<fstream>
#include	<sstream>
#include	<cstdio>
#include	<cstdlib>
#include	<string>
#include <sys/stat.h>
#include <sys/types.h>
#include <libgen.h>
#include <string.h>

#include <zip.h>



using namespace std;
using std::stringstream;





void s2s( ostream &out, istream &in, int size_buffer=10000){
	char aux[size_buffer];
	int clen;
	in.read(aux, sizeof(aux) );
	clen=in.gcount();
	while( clen!=0 ){
		out.write(aux, clen);
		in.read(aux, sizeof(aux) );
		clen=in.gcount();
	}
}



bool unzip( const string &zip_file, const string &dir ){

	int error=0;
	struct zip *file=zip_open(zip_file.c_str(), 0, &error);

	if( file ){
		int i=0;
		struct zip_file *fp_in=zip_fopen_index(file, i, 0);
		while( fp_in ){
			char out_filename[1000];
			strcpy(out_filename, zip_get_name(file, i, 0) );

			mkdir( dirname( (char*)out_filename) , 0777 );

			stringstream s_in;
			char aux[10000];

			int lido=zip_fread( fp_in, aux, sizeof(aux) );
			s_in.write(aux, lido);
			while( lido>0 ){
				lido=zip_fread( fp_in, aux, sizeof(aux) );
				s_in.write(aux, lido);
			}

			ofstream f_out( (dir+"/"+zip_get_name(file, i, 0)).c_str() );
			s2s( f_out, s_in);
			++i;
			fp_in=zip_fopen_index(file, i, 0);
		}
	}else{
			return false;
	}
	return true;
}

int main( int argc, char *argv[] ){
	unzip( string(argv[1]), string(argv[2]) );
	return 0;
}

