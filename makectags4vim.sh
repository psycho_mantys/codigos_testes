#!/bin/bash
#
# makectags.sh -Gera tags stl e boost usadas para auto-completar o C++ no vim.
#
# Site      : 
# Autor     : Psycho Mantys < psycho.mantys (,a.t.) gmail.com >
# -----------------------------------------------------------------------------
# 
#  Ao ser executado, baixara arquivos necess�rios para gerar as tags do vim 
# e as gera nos arquivos definidos pelas vari�veis TAG_OUT_STL e 
# TAG_OUT_BOOST (padr�o /tmp/stl.tags e /tmp/boost.tags respectivamente).
#  Esses arquivos devem ser usado com o vim.
#
# -----------------------------------------------------------------------------
# exemplo:
#
# > ./makectags
# > ls -1 /tmp/boost.tags /tmp/stl.tags
# /tmp/boost.tags
# /tmp/stl.tags
# > TAG_OUT_STL=/stl TAG_OUT_BOOST=/boost ./makectags
# > ls -1 /boost /stl
# /boost
# /stl
#
#==============================================================================
# ChangeLog:
#
# VERS�O:00.01:2008-04-06:Psycho:
# - Vers�o inaugural com cabe�alho adicionado.
# - Definidas valor padr�o para vari�veis de sa�da.
#
#==============================================================================
#
# Licen�a:GPL




TAGOUTBOOST=${TAG_OUT_BOOST:="/tmp/boost.tags"}
TAGOUTSTL=${TAG_OUT_STL:="/tmp/stl.tags"}
TMPFILE="`mktemp -d`" || exit 1

CWD="`pwd`"

cd "${TMPFILE}"

#make stl ctags
wget -c 'http://www.sgi.com/tech/stl/stl.tar.gz'
tar -xvf stl.tar.gz
for f in `ls` ; do
	sed -i s'/__STL_BEGIN_NAMESPACE/namespace std \{/' ${f} ;
	sed -i s'/__STL_END_NAMESPACE/\}/' ${f} ;
done

rm "${TAGOUTSTL}"
ctags -R --c++-kinds=+p --fields=+iaS --extra=+q -f /tmp/stl.tags.tmp ./
grep -v 'operator' /tmp/stl.tags.tmp > "${TAGOUTSTL}"


#make boost ctags
cp -rf /usr/include/boost ./

#ajusta macros. Namespaces com macros no lugar de nomes.
cd boost/filesystem

find -name "*.hpp" -exec sed -i s'/BOOST_FILESYSTEM_NAMESPACE/filesystem/' '{}' '+'

cd "../test"
find -name "*.hpp" -exec sed -i s'/BOOST_RT_PARAM_NAMESPACE/runtime/' '{}' '+'

cd "../signals"
find -name "*.hpp" -exec sed -i s'/BOOST_SIGNALS_NAMESPACE/signals/' '{}' '+'

cd "../"
find -name "*.hpp" -exec sed -i s'/BOOST_STD_EXTENSION_NAMESPACE/std/' '{}' '+'

cd "${TMPFILE}"
ctags -R --c++-kinds=+p --fields=+iaS --extra=+q -f /tmp/boost.tags.tmp boost/

#exclue muitas tags inuteis. No grep saem coisas inuteis e nao usadas. No while read line tirasse coisas com assinaturas iquais(revisar).
egrep -v -e '^FUSION' -e '::_bi::' -e '.*_HPP' -e '^BOOST' -e 'operator' -e 'detail/' -e 'boost/typeof' -e 'aux_/' -e '^[a-z][0-9]' -e '^[a-z][a-z][0-9]' -e '^vector[0-9]' -e '^[A-Z][0-9]' -e '^[A-Z][A-Z][0-9]' /tmp/boost.tags.tmp | (

line2="nops"

while read line ; do
	if [[ "x`eval cut -f1 <<< "${line2}" `" != "x`eval cut -f1 <<< "${line}" `" ]] ; then
		echo "${line}" #>> ${TAGOUTBOOST} ;
		line2=${line}
	fi
done

) > ${TAGOUTBOOST}

rm /tmp/boost.tags.tmp

cd "${CWD}"

rm -rf "${TMPFILE}"

#TODO:
#Vale a pena mesmo tirar no grep todos os 'operator' ?
#como se livrar de "algumaCoisa[0-9]{a-zA-Z}*" ?
#boost/preproc* vale a pena?
