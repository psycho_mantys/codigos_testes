#include <ctime>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>

// save matrix C in a file base.out
void save ( char *base, double *C, int N );

extern "C"
{
// Function that do the math: YOU have to write it!
void Multiply ( double *A, double *B, double *C, int *N );
}

// Simple program to multiply square full matrices: C = A*B
int main ( int argc, char *argv[] )
{
	// time, in clocks
	double t;

	if ( argc < 2 )
	{
		fprintf ( stderr, "Usage: %s N [file.out]\n\twhere N is an integer\n", argv[0] );
		exit(1);
	}

	// N is the order os matrices
	int N = atoi(argv[1]);
	if ( N < 2 )
	{
		fprintf ( stderr, "Error: N shoud be at least 2\n" );
		exit(1);
	}

	// Allocating room for matrices
	double *A = (double *) calloc ( N*N, sizeof(double) );
	double *B = (double *) calloc ( N*N, sizeof(double) );
	double *C = (double *) calloc ( N*N, sizeof(double) );

	if ( !A || !B || !C )
	{
		fprintf ( stderr, "Error: Couldn't do the allocation\n" );
		exit(1);
	}

	// Fill A nad B with some random values
	for ( int i=0; i<N; i++ )
	{
		for ( int j=0; j<N; j++ )
		{
			// Note that A[i][j] == A[N*i+j];
			A[i*N+j] = double(rand()) / RAND_MAX;
			B[i*N+j] = double(rand()) / RAND_MAX;
		}
	}

	// Starting with naive algorithm
	fprintf ( stderr, "Time: " );

	// Getting time of start
	t = clock();
	Multiply ( A, B, C, &N );
	t = clock() - t;
	fprintf ( stderr,"%5.2lfs\n", t/CLOCKS_PER_SEC );

	// if we supply a file name thea save C into this file
	if ( argc > 2 )
		save ( argv[2], C, N );

	return 0;
}

//
// save saves the matrix C(NxN) into file base.out
//

void save ( char *sName, double *C, int N )
{
/*
	// Default extension
	const char *sExt = ".out";

	// Size of final file name
	int nSize = strlen(base) + strlen(sExt) + 1;

	// Assembly final name from base + sExt
	char *sName = (char *) malloc ( nSize );
	sName = strcpy ( sName, base );
	sName = strcat ( sName, sExt );
*/

	// Try to open the file for writing
	FILE *Out = fopen ( sName, "wb" );
	if ( !Out )
	{
		fprintf ( stderr, "Couldn't open file %s for writing. Quitting!\n", sName );
		exit(1);
	}

	// Write a line at once to maximize I/O Speed
	for ( int i=0; i<N; i++ )
	{
		size_t nBytes;
		nBytes = fwrite ( C+N, sizeof(double), N, Out );
		if ( nBytes != N )
		{
			fprintf ( stderr, "Error writing to file %s. Tried to write %d doubles but only %d could be written!\n", 
								sName, N, nBytes );
			exit(1);
		}
	}

	fclose(Out);
}
