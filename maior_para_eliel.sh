#!/bin/bash

MAIOR_NOME=
MAIOR_NUM=0.0
MEDIA=0

SCALE=3

flt_test(){
	if [ "$( bc <<<"$*" )" = 1 ] ; then
		return 0
	else
		return 1
	fi
}

find . -iname "*.txt" | while read x ; do
	MAIOR_NUM=-100000.0
	MEDIA=0
	while read PRIMEIRO SEGUNDO ; do
		if flt_test "${MAIOR_NUM} < ${SEGUNDO}" ; then
			#echo exec
			#MAIOR_NOME="${x}"
			MAIOR_NUM="${SEGUNDO}"
		fi
	done < <( sed -e 's/\r//g' "${x}" )

	while read PRIMEIRO SEGUNDO ; do
		MEDIA=$( bc<<<"scale=$SCALE;${SEGUNDO}+${MEDIA}" )
	done < <( sed -e 's/\r//g' "${x}" | head -n 10 )

	while read PRIMEIRO SEGUNDO ; do
		MEDIA=$( bc<<<"scale=$SCALE;${SEGUNDO}+${MEDIA}" )
	done < <( sed -e 's/\r//g' "${x}" | tail -n 10 )

	echo -e "$x\t$( tr '.' ',' <<<"$MAIOR_NUM\t$( bc<<<"scale=$SCALE;${MEDIA}/20" )")"
done

##echo "${MAIOR_NOME}"
