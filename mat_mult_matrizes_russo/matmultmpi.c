#include <mpi.h>

#include <ctime>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>



#define FROM_MASTER 1
#define FROM_WORKER 2


// save matrix C in a file base.out
void save ( char *base, double *C, int N );

// Simple program to multiply square full matrices: C = A*B
int main ( int argc, char *argv[] )
{
	// time, in clocks
	double t;

	int nSize;
	int nRank;
	
	// MPI Initialization
	MPI_Init ( &argc, &argv );
	MPI_Comm_rank ( MPI_COMM_WORLD, &nRank );
	MPI_Comm_size ( MPI_COMM_WORLD, &nSize );


	if ( nRank == 0 )		// Master Process
	{
		if ( argc < 2 )
		{
			fprintf ( stderr, "Usage: %s N [file.out]\n\twhere N is an integer\n", argv[0] );
			MPI_Abort( MPI_COMM_WORLD, 1 );
		}
	
		// N is the order os matrices
		int N = atoi(argv[1]);
		if ( N < 2 )
		{
			fprintf ( stderr, "Error: N shoud be at least 2\n" );
			MPI_Abort( MPI_COMM_WORLD, 1 );
		}
	
		// Allocating room for matrices
		double *A = (double *) calloc ( N*N, sizeof(double) );
		double *B = (double *) calloc ( N*N, sizeof(double) );
		double *C = (double *) calloc ( N*N, sizeof(double) );
	
		if ( !A || !B || !C )
		{
			fprintf ( stderr, "Error: Couldn't do the allocation\n" );
			MPI_Abort( MPI_COMM_WORLD, 1 );
		}
	
		// Fill A nad B with some random values
		for ( int i=0; i<N; i++ )
		{
			for ( int j=0; j<N; j++ )
			{
				// Note that A[i][j] == A[N*i+j];
				A[i*N+j] = double(rand()) / RAND_MAX;
				B[i*N+j] = double(rand()) / RAND_MAX;
			}
		}
	
		// Starting with naive algorithm
		fprintf ( stderr, "Time: " );
	
		// Getting time of start
		t = clock();

		// Find a good slice size to send to each process
		int nWork = nSize - 1;
		int nRows = N / nWork;
		int nOffset = 0;
		MPI_Status nStatus;

		if ( N % nWork )
			nRows++;

		// Send a slice of A, and all B
		for ( int i=1; i<=nWork; i++ )
		{
			// Adjust nRows for last worker
			if ( N-nOffset < nRows )
				nRows = N-nOffset;

			// Send the order of matrices
			MPI_Send ( &N, 1, MPI_INT, i, FROM_MASTER, MPI_COMM_WORLD );
	
			// Send offset of lines
			MPI_Send ( &nOffset, 1, MPI_INT, i, FROM_MASTER, MPI_COMM_WORLD );
			
			// Send number of rows
			MPI_Send ( &nRows, 1, MPI_INT, i, FROM_MASTER, MPI_COMM_WORLD );
	
			// Send a slice of A
			MPI_Send( A+(nOffset*N), nRows*N, MPI_DOUBLE, i, FROM_MASTER, MPI_COMM_WORLD);
	
			// Send whole B
			MPI_Send ( B, N*N, MPI_DOUBLE, i, FROM_MASTER, MPI_COMM_WORLD );
	
			nOffset += nRows;
		}

		// Then wait for data from workers
		for ( int i=1; i<=nWork; i++ )
		{
			// Receive actual offset
			MPI_Recv ( &nOffset, 1, MPI_INT, i, FROM_WORKER, MPI_COMM_WORLD, &nStatus );
	
			// and how many lines were calculated
			MPI_Recv ( &nRows,   1, MPI_INT, i, FROM_WORKER, MPI_COMM_WORLD, &nStatus );

			// and then put them in C
			MPI_Recv ( C+(nOffset*N), nRows*N, MPI_DOUBLE, i, FROM_WORKER, MPI_COMM_WORLD, &nStatus );
		}

		t = clock() - t;
		fprintf ( stderr,"%5.2lfs\n", t/CLOCKS_PER_SEC );
	
		// if we supply a file name thea save C into this file
		if ( argc > 2 )
			save ( argv[2], C, N );
	}
	else		// Slave process
	{
		int N, nOffset, nRows;
		MPI_Status nStatus;

		// Receive info about which slice and size of matrix
		MPI_Recv ( &N,       1, MPI_INT, 0, FROM_MASTER, MPI_COMM_WORLD, &nStatus );
		MPI_Recv ( &nOffset, 1, MPI_INT, 0, FROM_MASTER, MPI_COMM_WORLD, &nStatus );
		MPI_Recv ( &nRows,   1, MPI_INT, 0, FROM_MASTER, MPI_COMM_WORLD, &nStatus );

		// Alloc the matrices
		double *A = (double *) calloc ( N*nRows, sizeof(double) );
		double *B = (double *) calloc ( N*N,     sizeof(double) );
		double *C = (double *) calloc ( N*nRows, sizeof(double) );
	
		if ( !A || !B || !C )
		{
			fprintf ( stderr, "Error: Couldn't do the allocation\n" );
			MPI_Abort( MPI_COMM_WORLD, 1 );
		}

		// Receive the data from master
		MPI_Recv ( A, N*nRows, MPI_DOUBLE, 0, FROM_MASTER, MPI_COMM_WORLD, &nStatus);
		MPI_Recv ( B, N*N,     MPI_DOUBLE, 0, FROM_MASTER, MPI_COMM_WORLD, &nStatus);


		// Finally, do the math
		for ( int i=0; i<nRows; i++ )
    		for ( int j=0; j<N; j++ )
				{
      			C[i*N+j] = 0.0;
      			for ( int k=0; k<N; k++ )
        			C[i*N+j] += A[i*N+k] * B[k*N+j];
      			}

		MPI_Send ( &nOffset, 1, MPI_INT,    0, FROM_WORKER, MPI_COMM_WORLD );
		MPI_Send ( &nRows,   1, MPI_INT,    0, FROM_WORKER, MPI_COMM_WORLD );
		MPI_Send ( C,  N*nRows, MPI_DOUBLE, 0, FROM_WORKER, MPI_COMM_WORLD );
	}
	
	MPI_Finalize();

	return 0;
}

//
// save saves the matrix C(NxN) into file base.out
//

void save ( char *sName, double *C, int N )
{
/*
	// Default extension
	const char *sExt = ".out";

	// Size of final file name
	int nSize = strlen(base) + strlen(sExt) + 1;

	// Assembly final name from base + sExt
	char *sName = (char *) malloc ( nSize );
	sName = strcpy ( sName, base );
	sName = strcat ( sName, sExt );
*/

	// Try to open the file for writing
	FILE *Out = fopen ( sName, "wb" );
	if ( !Out )
	{
		fprintf ( stderr, "Couldn't open file %s for writing. Quitting!\n", sName );
		exit(1);
	}

	// Write a line at once to maximize I/O Speed
	for ( int i=0; i<N; i++ )
	{
		size_t nBytes;
		nBytes = fwrite ( C+N, sizeof(double), N, Out );
		if ( nBytes != N )
		{
			fprintf ( stderr, "Error writing to file %s. Tried to write %d doubles but only %d could be written!\n", 
								sName, N, nBytes );
			exit(1);
		}
	}

	fclose(Out);
}
