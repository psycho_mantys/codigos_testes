
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#ifndef MY_NODE_PSYCHO
#define MY_NODE_PSYCHO

template <typename T>
class node{
		node<T> *_next;
		node<T> *_prev;
		T _data;
	public:
		node(){ }

		node( T &in, node<T> *n=NULL ): _next(n),
				_data(in)
		{ }

		node<T> *next(){
			return _next;
		}

		void next(node<T> *in){
			_next=in;
		}

		T &data(){
			return _data;
		}

		void data( T &in){
			_data=in;
		}
};

template <typename T>
class d_node{
		d_node<T> *_next;
		d_node<T> *_prev;
		T _data;
	public:
		d_node(){ }

		d_node( T &in, d_node<T> *n=NULL, d_node<T> *p=NULL ) :
				_next(n),
				_prev(n),
				_data(in)
		{ }

		d_node<T> *next(){
			return _next;
		}

		d_node<T> *prev(){
			return _prev;
		}

		void prev(d_node<T> *in){
			_prev=in;
		}

		void next(d_node<T> *in){
			_next=in;
		}

		T &data(){
			return _data;
		}

		void data( T &in){
			_data=in;
		}
};

#endif // ifdef MY_NODE_PSYCHO

#ifndef MY_STACK_PSYCHO
#define MY_STACK_PSYCHO

template <typename data>
class stack {
	typedef node<data> Node;

	Node *begin;
	unsigned int size;
	public:
		stack() : begin(NULL), size(0)
		{
		}

		bool is_empty(){
			return begin==NULL;
		}

		data pop(){
			if( begin ){
				data ret = begin->data();
				Node *n=begin->next();
				delete begin;
				begin=n;
				--size;
				return ret;
			}else{
				printf("cabou pilha\n");
				exit(-1);
			}
		}

		void push( data in ){
			Node *n( new Node(in,begin) );
			begin = n ;
			++size;
		}
};

#endif // ifdef MY_STACK_PSYCHO


using namespace std;

int main(){

	unsigned int qtd, i=0;

	scanf("%i%c",&qtd) ;

	for( i=0 ; i<qtd ; ++i ){
		string in ;
		stack<char> s;
		unsigned int index;
		getline (cin,in);
		for( index=0 ; index < in.size() ; ++index ){
			if( '[' == in.at(index) || '(' == in.at(index) ){
				s.push(in.at(index));
			}else{
				if( in.at(index) == ']' ){
					if( s.is_empty() || s.pop() != '[' ){
						break;
					}
				}else{
					if( s.is_empty() || s.pop() != '(' ){
						break;
					}
				}
			}
		}

		if( index==in.size() && s.is_empty() ){
			cout << "Yes" << endl ;
		}else{
			cout << "No"  << endl ;
		}
	}
	return 0;
}

