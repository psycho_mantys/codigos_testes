#include <ctime>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>

// Simple program to compare two files with matrix data
int main ( int argc, char *argv[] )
{

	if ( argc < 2 )
	{
		fprintf ( stderr, "Usage: %s file1 file2\n", argv[0] );
		exit(1);
	}

	FILE *f1 = fopen ( argv[1], "rb" );
	if ( !f1 )
	{
		fprintf ( stderr, "Couldn't open file %s for reading. Quitting!\n", argv[1] );
		exit(1);
	}

	FILE *f2 = fopen ( argv[2], "rb" );
	if ( !f2 )
	{
		fprintf ( stderr, "Couldn't open file %s for reading. Quitting!\n", argv[2] );
		exit(1);
	}

	// S is the sum of squared error
	double S = 0.0;
	while ( !feof(f1) && !feof(f2) )
	{
		double d1, d2;
	
		// read next value from f1 into d1
		fread ( &d1, sizeof(double), 1, f1 );
		
		// read next value from f2 into d2
		fread ( &d2, sizeof(double), 1, f2 );
	
		S += (d1-d2) * (d1-d2);
	}

	if ( !feof(f1) && !feof(f2) )
	{
		fprintf ( stderr, "Files %s and %s don't have the same size!\n", argv[1], argv[2] );
		exit(1);
	}

	fprintf ( stderr, "Sum of squared error: %lf\n", S );

	return 0;
}
