#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>


int Create_daemon(){
	if ( fork()==0 ){
		/* Cria nova sessao e sera lider dessa sessao */
		setsid();
		/* Diz para os sinais de SIGHUP serem ignorados é */
		signal(SIGHUP , SIG_IGN);
		if( fork()==0 ){
			/* Muda diretorio para / */
			chdir("/");
			/* Da ao processo filho completo dominio de suas permissoes */
			umask(0);
			/* Rediretciona stdout, stdin e stderr para null */
			close(0);
			close(1);
			close(2);
			fopen(NULL,"r");
			fopen(NULL,"rw");
			fopen(NULL,"rw");
		}else{
			_exit(0);
		}
	}else{
		_exit(0);
	}
	return 0;
}

int main(){
	Create_daemon();
	printf("oi");
	sleep(10);
	return 0;
}
