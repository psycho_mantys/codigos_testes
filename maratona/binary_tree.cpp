/*
 * =====================================================================================
 *
 *       Filename:  binary_tree.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  13-11-2010 14:14:07
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys (P.M.), psycho.mantys(.AT,)gmail dot com
 *        Company:  LCCV
 *
 * =====================================================================================
 */

#ifndef MY_BINARY_TREE_PSYCHO
#define MY_BINARY_TREE_PSYCHO

#include <memory>
#include "node.cpp"

template <typename data_t>
class binary_tree {
	private:
//	public:
		typedef d_node<data_t> Node;

		auto_ptr<Node> begin;
		unsigned int size;

	public:
		binary_tree() :
			begin(NULL),
			size(0)
		{ }

		inline bool is_empty(){
			return size==0;
		}

		inline Node *find_node( const data_t &in ){
			return find_node( in, begin.get() );
		}

		inline Node *find_node( const data_t &in, Node* atual ){
			if( atual ){
				if( in==atual->data() ){
					return atual;
				}
				if( in<atual->data() ){
					return find_node( in, atual->left() );
				}else{
					return find_node( in, atual->rigth() );
				}
			}
			return NULL;
		}

		inline data_t *find( const data_t &in ){
			return find(in, begin.get());
		}

		inline data_t *find( const data_t &in, Node* atual ){
			Node *ret=find_node(in);
			if( ! ret )
				return NULL;
			return &ret->data();
		}

//		inline void remove_node( const data_t &in, Node* atual=NULL ){
//			if( ! atual )
//				atual=this->begin.get();
//
//			
//		}

		inline void insert( const data_t &in ){
			Node *n( new Node(in) );

			if( this->is_empty() ){
				begin.reset(n);
			}else{
				insert( n, begin.get() );
			}

			++size;
		}

		inline void insert( Node *n, Node *atual ){
			while( atual ){
				if( n->data()<atual->data() ){
					if( ! atual->left() ){
						atual->left( n );
						return;
					}else{
						atual=atual->left();
					}
				}
				if( atual->data()<n->data() ){
					if( ! atual->rigth() ){
						atual->rigth( n );
						return;
					}else{
						atual=atual->rigth();
					}
				}
			}
		}

		void free_root( Node *n ){
			if( n ){
				free_root( n->left() );
				free_root( n->rigth() );
				delete n;
			}
		}

		unsigned int altura(){
			return this->altura( begin.get() );
		}

		unsigned int altura( Node *atual ){
			if( ! atual )
				return 0;

			unsigned const int n_l=altura( atual->left() );
			unsigned const int n_r=altura( atual->rigth() ) ;
			return 1+(n_r<n_l?n_l:n_r) ;
		}

		void print_order(){
			print_order( begin.get() );
		}

		void print_order( Node *n ){
			#ifdef DEBUG
			cout << n->data() << endl ;
			if ( n->left()!=NULL ){
				print_order( n->left() );
			}
			if ( n->rigth()!=NULL ){
				print_order( n->rigth() );
			}
			#endif
		}

		~binary_tree(){
			free_root( begin.release() );
		}
};

#endif // ifdef MY_BINARY_TREE_PSYCHO

