/*
 * =====================================================================================
 *
 *       Filename:  sentido_da_vida.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05-04-2011 19:01:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys (P.M.), psycho.mantys(.AT,)gmail dot com
 *        Company:  LCCV
 *
 * =====================================================================================
 */

#include "soapH.h" 
#include "SentidoDaVida.nsmap" 
#include <math.h>

int main(){ 
	soap_serve( soap_new() ); // call the incoming remote method request dispatcher 
} 

// Implementation of the "add" remote method: 
int ns__SentidoDaVida(struct soap *soap, double &result){
	result=42;
	return SOAP_OK;
}

