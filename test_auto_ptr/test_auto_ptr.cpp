/*
 * =====================================================================================
 *
 *       Filename:  test_auto_ptr.cpp
 *
 *    Description:  Teste de auto_ptr comparado a um ponteiro normal.
 *
 *        Version:  1.0
 *        Created:  12/10/05 07:55:10
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys (P.M.), psycho.mantys(.AT,)gmail dot com
 *
 * =====================================================================================
 */
/* #####   HEADER FILE INCLUDES   ################################################### */
#include	<memory>
#include	<cstdlib>
#include	"tempo.hpp"

/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE   ################################### */
#define	MAGIC_NUMBER	1234

using std::auto_ptr ;

int main( int argc, char *argv[] ){
	puts("Tempo de criação e destruição:");
	/* Testa cração, alocação e desalocação */
	{
		volatile MedeTempo criacao("Ponteiro normal:") ;
		for ( int j=0 ; j<10000 ; ++j)
			for ( int i=0 ; i<100000 ; ++i ){
				int *x=new int;
				delete x ;
			}
	}
	{
		volatile MedeTempo criacao("Ponteiro automatico:") ;
		for ( int j=0 ; j<10000 ; ++j)
			for ( int i=0 ; i<100000 ; ++i)
				auto_ptr<int> x(new int);
	}
	/* Teste de escrita a posição de memoria */
	{
		auto_ptr<volatile int> x(new int);
		volatile MedeTempo Escrita("auto_ptr:\nTeste de escrita:");
		for ( long int i=0 ; i<100000000000 ; ++i){
			*x = MAGIC_NUMBER ;
		}
	}
	{
		volatile int *x = new int;
		volatile MedeTempo Escrita("ptr normal:\nTeste de escrita:");
		for ( long int i=0 ; i<100000000000 ; ++i){
			*x = MAGIC_NUMBER ;
		}
		delete x;
	}
	/* Teste de acesso a posição de memoria */
	{
		auto_ptr<int> x(new int);
		volatile int aux;
		*x = MAGIC_NUMBER;

		volatile MedeTempo acesso("auto_ptr:\nTeste de acesso:");
		for ( long int i=0 ; i<50000000000 ; ++i){
			aux = *x ;
		}
	}
	{
		int *x = new int;
		volatile int aux;
		*x = MAGIC_NUMBER;

		volatile MedeTempo acesso("ptr normal:\nTeste de acesso:");
		for ( long int i=0 ; i<50000000000 ; ++i){
			aux = *x ;
		}
		delete x;
	}

	return EXIT_SUCCESS ;
}	/* ----------  end of function main  ---------- */

