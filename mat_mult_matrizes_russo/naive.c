// A, B and C are all (NxN) matrices

#include <string.h>
#include <stdio.h>

extern "C"
{
void Multiply ( double *A, double *B, double *C, int *M )
{
	int N = *M;
	memset(C,0,N*N);
	// Perform the matrix multiplication
	for ( int i=0; i<N; i++ )
	{
		for ( int k=0; k<N; k++ )
		{
//			C[i*N+j] = 0.0;
			for ( int j=0; j<N; j++ )
				C[i*N+j] += A[i*N+k] * B[k*N+j];
		}
	}
}
}
