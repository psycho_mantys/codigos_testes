#!/bin/bash
#
# adiciona e remove palavras e sites no squid
# version 0.1 by but3k4 --> but3k4@onerd.com.br
#
# Como usar:
# 
# Crie as seguintes acls no seu squid.conf:
# 
# acl porn url_regex "/etc/squid/porn.txt"
# acl sites dstdomain "/etc/squid/sites.txt"
# http_access deny porn 
# http_access deny sites
# 
# Depois crie o usu�rio squid:
#
# useradd -m -s /bin/bash squid
# passwd squid
#
# Cadastre esse usuario no /etc/sudoers:
#
# squid  ALL=(ALL)       NOPASSWD: ALL
#
# Crie um .bash_profile no home do usuario squid com o seguinte conte�do:
#
# sudo admin_squid.sh
# exit
#
# Depois mova este script para o /usr/local/bin e bom uso.
#
dir_squid="/etc/squid"
tmp="/tmp/squid"
#
trap "" 2 3
trap "" 20

if [ ! -d $tmp ]; then
	mkdir $tmp
fi

choice() {

options=$(dialog --stdout --backtitle "Vers�o 0.1" --title "Gerenciador de regras do squid" \
        --radiolist "\nO que voc� deseja fazer?\n" 0 0 0 \
1 "Bloquear uma palavra" off \
2 "Bloquear um site" off \
3 "Remover uma palavra bloqueada" off \
4 "Remover um site bloqueado" off \
5 "Visualizar os itens bloqueados" off \
6 "Sair " off 2>&1)

case $options in
'')
default
;;
'1')
add_palavra
;;
'2')
add_site
;;
'3')
rm_palavra
;;
'4') 
rm_site
;;
'5')
ver_bloqueados
;;
'6')
sair
;;
*)
;;
esac
}


default() {
if [ $? = 0 ]; then
	dialog --title "Aten��o" \
		--msgbox "\n\nEscolha uma op��o no menu principal" 9 39
	choice
else
	sair
fi
}

add_palavra() {
dialog --title "Adicionando palavras" \
	--inputbox "\nDigite a palavra que voc� deseja bloquear:" 11 34 2>$tmp/word.txt
	if [ $? = 1 ]; then
		choice
	fi
	if [ "`cat $tmp/word.txt`" == ""  ]; then
		dialog --title "ERRO" \
			--msgbox "\n\n    Nenhuma palavra foi digitada" 9 42
	else
		if [ "`cat $tmp/word.txt`" = "`cat $tmp/word.txt | grep -f $dir_squid/porn.txt`" ]; then
			dialog --title "Aten��o" --msgbox "\n\n  Esta palavra j� est� bloqueada" 9 40
			choice
		else
			echo `cat $tmp/word.txt` >> $dir_squid/porn.txt
			dialog --title "Palavra adicionada com sucesso" \
				--msgbox "\n\nPressione <ENTER> para voltar ao menu principal" 9 51
		fi
	fi
	choice
}

rm_palavra() {
dialog --title "Removendo palavras" \
	--inputbox "\nDigite a palavra que voc� deseja remover:" 11 34 2>$tmp/rword.txt
	if [ $? = 1 ]; then
		choice
	fi
	if [ "`cat $tmp/rword.txt`" == ""  ]; then
		dialog --title "ERRO" \
			--msgbox "\n\n    Nenhuma palavra foi digitada" 9 42
	else
		if [ "`cat $tmp/rword.txt | grep -f $dir_squid/porn.txt`" == "" ]; then
			dialog --title "Aten��o" --msgbox "\n\n  Esta palavra n�o est� bloqueada" 9 42
			choice
		else
			cat $dir_squid/porn.txt | grep -v `cat $tmp/rword.txt` >> $tmp/porn.tmp
			mv $tmp/porn.tmp $dir_squid/porn.txt
			dialog --title "Palavra removida com sucesso" \
				--msgbox "\n\nPressione <ENTER> para voltar ao menu principal" 9 52
		fi
	fi
	choice
}

add_site() {
dialog --title "Bloqueando sites" \
	--inputbox "\nDigite o site que voc� deseja bloquear:" 11 34 2>$tmp/addsite.txt
	if [ $? = 1 ]; then
		choice
	fi
	if [ "`cat $tmp/addsite.txt`" == ""  ]; then
		dialog --title "ERRO" \
			--msgbox "\n\n    Nenhuma site foi digitado" 9 40
	else
		if [ "`cat $tmp/addsite.txt`" = "`cat $tmp/addsite.txt | grep -f $dir_squid/sites.txt`" ]; then
			dialog --title "Aten��o" --msgbox "\n\n   Este site j� est� bloqueado" 9 38
			choice
		else
			echo `cat $tmp/addsite.txt` >> $dir_squid/sites.txt
			dialog --title "Site bloqueado com sucesso" \
				--msgbox "\n\nPressione <ENTER> para voltar ao menu principal" 9 52
		fi
	fi
	choice
}

rm_site() {
dialog --title "Removendo sites" \
	--inputbox "\nDigite o site que voc� deseja remover:" 11 34 2>$tmp/delsite.txt
	if [ $? = 1 ]; then
		choice
	fi
	if [ "`cat $tmp/delsite.txt`" == ""  ]; then
		dialog --title "ERRO" \
			--msgbox "\n\n    Nenhuma site foi digitado" 9 40
	else
		if [ "`cat $tmp/delsite.txt | grep -f $dir_squid/sites.txt`" == "" ]; then
			dialog --title "Aten��o" --msgbox "\n\n   Este site n�o est� bloqueado" 9 39
			choice
		else
			cat $dir_squid/sites.txt | grep -v `cat $tmp/delsite.txt` >> $tmp/sites.tmp
			mv $tmp/sites.tmp $dir_squid/sites.txt
			dialog --title "Site removido com sucesso" \
				--msgbox "\n\nPressione <ENTER> para voltar ao menu principal" 9 52
		fi
	fi
	choice
}

ver_bloqueados() {

options=$(dialog --stdout --title "Visualizador de bloqueios" \
        --radiolist "\nQual tipo de arquivo voc� deeseja ver?\n" 0 0 0 \
1 "Visualizar palavras bloqueadas" off \
2 "Visualizar sites bloqueados" off \
3 "Voltar" off 2>&1)

case $options in
'')
default
;;
'1')
ver_palavra
;;
'2')
ver_site
;;
'3')
choice
;;
*)
;;
esac
}

ver_palavra() {
	more $dir_squid/porn.txt
	echo -en "\nPressione <ENTER> para voltar ao menu principal "
	read
	choice
}

ver_site() {
	more $dir_squid/sites.txt
	echo -en "\nPressione <ENTER> para voltar ao menu principal "
	read
	choice
}

sair() {
	dialog --title "Saindo" --infobox "\n\nSaindo do gerenciador de regras do squid\n" 8 44
	if [ -x /etc/rc.d/rc.squid ]; then
	/etc/rc.d/rc.squid restart
	fi
	rm -rf $tmp
	sleep 1
	exit 0
}

choice
