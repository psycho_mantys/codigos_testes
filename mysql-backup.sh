#!/bin/sh
#
USER="admin"
PASS="8xvcB647zx"
HOST="localhost"
BACKUPDIR="/backup/dumps"
DATE="`date +%Y%m%d`"
OPT="--compatible=mysql323 --compress --set-charset=latin1"
VERBOSE="y"

dump() {
   if [ "$VERBOSE" == "y" ]; then
      echo "Fazendo backup da database $1, tabela $2"
   fi
   mysqldump --user=$USER --password=$PASS --host=$HOST $OPT $1 $2 > $3
}

list_databases() {
   mysql --user=$USER --password=$PASS --host=$HOST --batch --skip-column-names -e "show databases" | sed 's/ \+//g'
}

list_tables() {
   mysql --user=$USER --password=$PASS --host=$HOST --batch --skip-column-names -D $1 -e "show tables" | sed 's/ \+//g'
}

compact() {
   if [ "$VERBOSE" == "y" ]; then
      echo "Compactando a tabela $1"
   fi
   if [ -e $1.gz ]; then
      if [ "$VERBOSE" == "y" ]; then
         echo "Removendo o arquivo $1.gz"
      fi
      rm $1.gz
   fi
   gzip $1
}

if [ ! -e $BACKUPDIR ]; then
   if [ "$VERBOSE" == "y" ]; then
      echo "Criando o diretorio $DATABASE"
   fi
   mkdir -p $BACKUPDIR
fi

for DATABASE in `list_databases`; do
   for TABLE in `list_tables $DATABASE`; do
      if [ ! -e "$BACKUPDIR/$DATABASE" ]; then
         if [ "$VERBOSE" == "y" ]; then
            echo "Criando o diretorio $DATABASE"
         fi
         mkdir -p $BACKUPDIR/$DATABASE
      fi
      if [ ! -e "$BACKUPDIR/$DATABASE/backup-$DATE" ]; then
         if [ "$VERBOSE" == "y" ]; then
            echo "Criando o diretorio backup-$DATE em $DATABASE"
         fi
         mkdir -p $BACKUPDIR/$DATABASE/backup-$DATE
      fi
      cd $BACKUPDIR/$DATABASE/backup-$DATE
      dump $DATABASE $TABLE $TABLE.sql
      compact $TABLE.sql
   done
done
