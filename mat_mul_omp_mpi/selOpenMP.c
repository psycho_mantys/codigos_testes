#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "omp.h"
#include "mpi.h"

struct time{
	
	double t_aloc;
	double t_leit;
	double t_proc;
	double t_comun;
	double t_total;
} Time;

int gaussJacobi(int n, double **A, double *b, double *x0, int nmaxite, double tol, double *x, int *nite, int NumThreads)
{
	double time;
	int i,j,nite2;
	double norma;
	int id;

	norma=tol+1;

	time = omp_get_wtime();
		#pragma omp parallel num_threads(NumThreads) private(nite2)
		{
		nite2=0;
		/*while (*nite<=nmaxite && norma>tol) {*/
		for (nite2=0; nite2<=nmaxite; nite2++) {


			double s;

			#pragma omp  for private (s)
			for (i=0;i<n;i++){

				s=0;

				for (j=0;j<n;++j)
					s+= (A[i][j]*x0[j]);

				s=s-(A[i][i]*x0[i]);

				x[i]=(b[i] - s)/A[i][i];
			}

			id = omp_get_thread_num();
	
			/*teste de convergencia*/
			norma=0;
			#pragma omp parallel num_threads(NumThreads)
			{		
			#pragma omp  for reduction(+:norma)
			for (i=0;i<n;i++){
				norma=norma+ (x[i]-x0[i])*(x[i]-x0[i]);
				x0[i]=x[i];
			}
			}
			norma=sqrt(norma);
		}
		*nite = nite2;
		}

	time = omp_get_wtime()-time;
	return 1;
}

int readDataFile(FILE *fp, double ***A, double **b, double **x0, double **x, int *nmaxite, double *tol) {

	int n =3, i, j;

	/*Pega o tamanho da matriz*/
	fscanf(fp,"%d",&n);

	/* /inicia a contagem do tempo de alocação*/
	Time.t_aloc = MPI_Wtime() ;

	/*Aloca a matriz e os vetores com base no n*/
	*A = calloc(n, sizeof(double *));
	for (i = 0; i < n; i++)
		(*A)[i] = calloc(n, sizeof(double));
	*b  = calloc(n, sizeof(double));
	*x0 = calloc(n, sizeof(double));
	*x = calloc(n, sizeof(double));

/*	//finaliza a contagem do tempo de alocação*/
	Time.t_aloc = MPI_Wtime() - Time.t_aloc ;

/*	//inicia a contagem do tempo de leitura do arquivo de entrada*/
	Time.t_leit = MPI_Wtime() ;

	/*Atribui os valores lidos do arquivo à matriz e aos vetores*/
	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			fscanf(fp, "%lf", &((*A)[i][j]));
	for(i = 0; i < n; i++)
		fscanf(fp, "%lf", &((*b)[i]));
	for(i = 0; i < n; i++)
		fscanf(fp, "%lf", &((*x0)[i]));
	fscanf(fp, "%d", nmaxite);
	fscanf(fp, "%lf", tol);

/*	//finaliza a contagem do tempo de alocação*/
	Time.t_leit = MPI_Wtime() - Time.t_leit ;

	return n;

}

/*int readDataFileBin(FILE *fp, double ***A, double **b, double **x0, double **x, int *nmaxite, double *tol) {

	int n, i, j;

	Pega o tamanho da matriz
	fscanf(fp,"%d",&n);

	Aloca a matriz e os vetores com base no n
	*A = calloc(n, sizeof(double *));
	for (i = 0; i < n; i++)
		*A[i] = calloc(n, sizeof(double));
	*b  = calloc(n, sizeof(double));
	*x0 = calloc(n, sizeof(double));
	*x  = calloc(n, sizeof(double));

	Atribui os valores lidos do arquivo à matriz e aos vetores
	for(i = 0; i < n; i++)
		fread(*A[i], sizeof(double), n, fp);
	fread(*b, sizeof(double), n, fp);
	fread(*x0, sizeof(double), n, fp);
	fread(*nmaxite, sizeof(int), 1, fp);
	fread(*tol, sizeof(double), 1, fp);

	return n;
}*/

int main(int argc,char *argv[]) {


	int    n;						/* Numero de Equacoes*/
	double **A;						/* Matriz dos Coeficientes*/
	double *b;						/* Vetor b*/
	double *x0;						/* Vetor x0*/
	double tol;						/* Tolerancia admitida*/
	int    nmaxite;						/* Numero maximo de iteracoes*/
	double *x;						/* Vetor solucao*/
	int    i;						/* Indexadores da matriz*/
	int    nite;						/* Numero total de iteracoes*/
	FILE   *fpin;						/* Ponteiro para o arquivo de dados*/
	int NumThreads;

/*	//Inicializa o MPI apenas para usar a funcao que mede tempo*/
	MPI_Init(&argc,&argv);
	
	NumThreads = atoi(argv[2]);

/*	//inicia a contagem do tempo total de execução*/
	Time.t_total = MPI_Wtime();

	/*Abre arquivo de entrada*/
	fpin = fopen(argv[1],"r");	
	if (fpin==NULL){
		fprintf(stdout,"Erro ao tentar abrir o arquivo de dados\n");
		fflush(stdout);
	}

	/*Leitura do arquivo de dados*/
	n = readDataFile(fpin, &A, &b, &x0, &x, &nmaxite, &tol);

/*	//inicia a contagem do tempo de processamento */
	Time.t_proc = MPI_Wtime() ;

	/*Chama a funcao que gerencia os processos*/
	gaussJacobi(n,A,b,x0,nmaxite,tol,x,&nite,NumThreads);


/*	//finaliza a contagem do tempo de processamento*/
	Time.t_proc = MPI_Wtime() - Time.t_proc ;
	
/*	// Impressao da solucao do sistema */
	fprintf(stdout,"A solucao do encontrada apos %d iteracoes foi:\n",nite);
	fprintf(stdout,"X[%d] = %4.4f\n",0,x[0]);
	fprintf(stdout,"X[%d] = %4.4f\n",n-1,x[n-1]);
	
	/* Fecha os arquivos de entrada e saida */
	fclose(fpin);

	/* Libera memoria alocada */
	for(i = 0; i < n; i++)
		if(A[i] != NULL) free(A[i]);
	if(A != NULL) free(A);
	if(x != NULL) free(x);
	if(b != NULL) free(b);
	if(x0 != NULL) free(x0);

/*	//finaliza a contagem do tempo total de execução*/
	Time.t_total = MPI_Wtime() -Time.t_total;

/*	//impressão dos tempos*/
	printf(" \n\nFIM DA EXECUCAO : ");
	printf( " \n\nNome do Executavel:%s",argv[0]);
	printf("\nNumero de Threads:%d",NumThreads);
	printf("\nArquivo de Entrada:%s",argv[1]);
	printf("\n\nTempo de Alocacao:%f",Time.t_aloc);
	printf("\nTempo de Leitura:%f",Time.t_leit);
	printf("\nTempo de Processamento:%f",Time.t_proc);
	printf("\nTempo total:%f\n\n",Time.t_total);	


/*	// Finaliza o MPI em todos os processos*/
	MPI_Finalize();

	return 0;	
}

