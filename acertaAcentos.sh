#!/bin/bash

chfileiso2utf(){
	new_name="$(iconv --from-code=UTF-8 --to-code=ISO-8859-1 <<<"$1")"
	if [ ! "${new_name}" = "$1" ] ; then
		echo $1
		echo $new_name
		mv "$1" "${new_name}"
	fi
}

if [ ! "$1" = '-r' ] ; then
	find "$1" -exec "$0" '-r' '{}' ';'
else
	chfileiso2utf "$2"
fi
