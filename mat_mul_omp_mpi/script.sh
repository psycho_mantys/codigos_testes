#!/bin/bash

HELP="
Usage: $0 [ commands ]

Commands:
	--help|-h|-?		: This help mensage.
	-m [programs]		: Make all programs or especific [ programs ]
	-a			: Make and run all.
	-r			: Run programs
	--no-mpi 		: Disable mpi.
	--mpi			: Enable mpi.
	--mpi-numt num		: Number of mpi threads
	--numthreads|-t num	: Number of local threads. (Valid fo pthread or/and openmp)
	--out | -o [out]	: File to redirect output. (default is INFO.out)
"

OUT=${OUT:=INFO.out}
TNUM=${TNUM:=2}
TNUMMPI=${TNUMMPI:=4}

if [ -w "${OUT}"  ] ; then
	rm "${OUT}"
fi

TEMP=`getopt -o 'h?am:t:o:' --long 'no-mpi,mpi,mpi-numt:,numthreads:,out,help' -n $0 -- "$@"`
eval set -- "$TEMP"

while true ; do
	case "${1}" in
		-a)
			MAKE="all"
			RUN="yes"
			MPI="yes"
			shift 
		;;
		-m)
			case "$2" in
				"")
					MAKE=all
				;;
				*)
					MAKE="$2"
				;;
			esac
			shift 2
		;;
		--no-mpi)
			MPI="no"
			shift 
		;;
		--mpi)
			MPI="yes"
			shift
		;;
		--mpi-numt)
			case "$2" in
				"")
					echo "Erro: $1 required arg"
					exit 1 ;
				;;
				*[a-bA-Z]*)
					echo "Erro: Invalid type in arg $2"
					exit 1
				;;
				[0-9]*)
					TNUMMPI=$2
				;;
			esac
			shift 2
		;;
		--numthreads|-t)
			case "$2" in
				"")
					echo "Erro: $1 required arg"
					exit 1 ;
				;;
				*[a-bA-Z]*)
					echo "Erro: Invalid type in arg $2"
					exit 1
				;;
				[0-9]*)
					TNUM=$2
				;;
			esac
			shift 2	
		;;
		--out|-o)
			case "$2" in
				"")
					echo "Erro: $1 required arg"
					exit 1 
				;;
				* )
				OUT=$2
				;;
			esac
			shift 2
		;;
		--)
			shift
			break
		;;
		-h|-?|--*)
			echo "${HELP}"
			exit 0 ;
		;;
		*)
			echo "Error: arg $1 no expect or valid"
			exit 1;
		;;
	esac
done

if [[ "x" != "x${MAKE}" ]] ; then
	make ${MAKE}
fi

echo Start Exec.

(
for EXEC in ./pThreadSEL ./selOpenMP ./selSequencial ; do
	#se na for executavel, nao executa :D
	if [ -x ${EXEC} ] ; then
		echo "########################################################"
		echo "#${EXEC}"
		for IN in sistema_500.dat sistema_1000.dat  sistema_2000.dat  sistema_4000.dat  sistema_8000.dat  sistema_15000.dat ; do
			#if arquivo pode ser lido, executa e le :D
			if [ -r ${IN} ] ; then
				echo "ARQ_ENTREDA    :${IN}"
				for N in `seq -w 1 5` ; do
					echo "Execucao Numero:${N}"
					${EXEC} ${IN} ${TNUM}
				done
			fi
		done
	fi
done

if [[ x"yes" == "x${MPI}" ]] ; then
for EXEC in ./SelMPI ./SelMPI-OMP ; do
	#se na for executavel, nao executa :D
	if [ -x ${EXEC} ] ; then
		echo "########################################################"
		echo "#${EXEC}"
		for IN in sistema_500.dat sistema_1000.dat  sistema_2000.dat  sistema_4000.dat  sistema_8000.dat  sistema_15000.dat ; do
			#if arquivo pode ser lido, executa e le :D
			if [ -r ${IN} ] ; then
				echo "ARQ_ENTREDA    :${IN}"
				for N in `seq -w 1 5` ; do
					echo "Execucao Numero:${N}"
					mpirun -np ${TNUMMPI} ${EXEC} ${IN} ${TNUM}
				done
			fi
		done
	fi
done
fi

) >> ${OUT}
