/*
 * =====================================================================================
 *
 *       Filename:  sentido_da_vida.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05-04-2011 19:36:09
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys (P.M.), psycho.mantys(.AT,)gmail dot com
 *        Company:  LCCV
 *
 * =====================================================================================
 */

#include "soapSentidoDaVidaProxy.h" // get proxy
#include "SentidoDaVida.nsmap" // get namespace bindings


SentidoDaVida ws;

int main()
{
	double r;
	ws.ns1__SentidoDaVida(r);
//	if (ws.ns1__SentidoDaVida(r) == SOAP_OK )
		std::cout << r << std::endl;
//	else
	soap_print_fault(ws.soap, stderr);
	return 0;
} 

