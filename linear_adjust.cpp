/* g++ linear_adjust.cpp -lgsl -lgslcblas -lboost_program_options */
#include	<iostream>
#include	<iomanip>
#include	<fstream>
#include	<vector>
#include	<string>
#include	<memory>
#include	<cstdlib>
#include	<gsl/gsl_fit.h>
#include	<boost/program_options.hpp>
#include	<gsl/gsl_randist.h>

using namespace std;
namespace po=boost::program_options;

int main( int argc, char *argv[] ){
/*	int n = 9;
	double x[9] = { 1,2,3,4,5,6,7,8,9 };
	double y[9] = { 2,3,5,4,6,7,9,0,10 };
	double c0, c1, cov00, cov01, cov11, sumsq;*/

	auto_ptr<ofstream> proxy_output_file;
	auto_ptr<ifstream> proxy_input_file;

	vector<double> X,Y;
	double c0, c1, cov00, cov01, cov11, sumsq;

	// Começa a tratar opções passadas ao programa
	po::options_description desc("Opções");
	desc.add_options()
		("help,h", "produce help message")
		("inicio,1", po::value<double>()->default_value(0.0),"Inicio do intervalo para fazer a normalização.")
		("fim,2", po::value<double>()->default_value(0.0),"Fim do intervalo para fazer a normalização.")
		("input,i", po::value<string>(), "Arquivo de entrada para o programa.")
		("output,o", po::value<string>(), "Arquivo de saida.")
		;

	po::positional_options_description p;
	p.add("input", -1);
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
	po::notify(vm);

	// Caso seja solicitado ajuda ou a entrada esteja faltando
	if( vm.count("help") ) {
		cout << desc << "\n";
		return 1;
	}

	if( vm.count("output") ){
		proxy_output_file.reset( new ofstream( vm["output"].as<string>().c_str() ) );
	}

	if( vm.count("input") ){
		proxy_input_file.reset( new ifstream(vm["input"].as<string>().c_str()) );
	}

	double inicio=vm["inicio"].as<double>(), fim=vm["fim"].as<double>();
	// Final do tratamento de opções

	istream &input_file=( vm.count("input") )?(*proxy_input_file):cin;
	ostream &output_file=( vm.count("output") )?(*proxy_output_file):cout;

	double aux1,aux2;
	input_file >> setprecision(20) >> aux1 >> aux2 ;
	int i=0;
	while( !input_file.eof() ){
		X.push_back(aux1);
		Y.push_back(aux2);
		cout << i++<<" "<< aux1 <<" "<< aux2 << endl;
		input_file >> setprecision(20) >> aux1 >> aux2 ;
	}

	double *x=&X[0],*y=&Y[0];
//	double x[9] = { 1,2,3,4,5,6,7,8,9 };double y[9] = { 2,3,5,4,6,7,9,0,10 };

	gsl_fit_linear(x, 1, y, 1, (int)(X.size()),
			&c0, &c1, &cov00, &cov01, &cov11,
			&sumsq);
	printf("# best fit: Y = %g + %g*X (sum=%g size=%d)\n",c0,c1,sumsq,X.size());
	printf ("# best fit: Y = %g + %g X\n", c0, c1);
	printf ("# covariance matrix:\n");
	printf ("# [ %g, %g\n#   %g, %g]\n",
			cov00, cov01, cov01, cov11);
	printf ("# sumsq = %g\n", sumsq);


	return 0;
}

