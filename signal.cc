#include	<iostream>
#include	<signal.h>
#include	<vector>
#include	<cstdlib>

using namespace std;

void f1() {
	cout << "calling f1()..." << endl;
}               

void f2() {
	cout << "calling f2()..." << endl;
}               

typedef void(*endFunc)(void);

vector<endFunc> endFuncs;

void cleanUp( int dummy ) {
	for( unsigned int i = 0; i < endFuncs.size(); i++ ) {
		endFunc f = endFuncs.at(i);
		(*f)();
	}
	sleep(1000);
	exit(-1);
}               

int main() {            

	// connect various signals to our clean-up function
	signal( SIGTERM, cleanUp );
	signal( SIGINT, cleanUp );
	signal( SIGQUIT, cleanUp );
	signal( SIGHUP, cleanUp );
	signal( SIGKILL, cleanUp );

	// add two specific clean-up functions to a list of functions
	endFuncs.push_back( f1 );
	endFuncs.push_back( f1 );
	endFuncs.push_back( f2 );             
	endFuncs.push_back( f2 );             

	// loop until the user breaks
	while( 1 ){  cout << "opa\n" ; }

	return 0;
}

