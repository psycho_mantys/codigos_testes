	subroutine Multiply (A, B, C, N)

	! Parameters
	integer N
	double precision A(N,N)
	double precision B(N,N)
	double precision C(N,N)

	call dgemm ( "N", "N", N, N, N, 1.0, B, N, A, N, 0.0, C, N )

	end