#!/bin/bash

if [[ $# < 2 ]] ; then
	echo "Use: $0 [ lista ] [ qtdDeLetras ]"
	exit 1 ;
fi

Tamanho=${#1}
lista={${1:${Count}:1}
for((Count=1 ; Count<${Tamanho} ;Count++ )) ; do
	lista="${lista},${1:${Count}:1}"
done
lista="${lista}}"

entrada='eval echo ${lista}'
for((Count=1 ; Count<${2} ;Count++ )) ; do
	entrada="$entrada"'${lista}'
done

eval ${entrada} | tr ' ' '\n' | while read linha; do
	if [[ "x" == "x"$( ispell -l -d br <<< "${linha}" ) ]] ; then
		echo ${linha} ;
	fi
done

