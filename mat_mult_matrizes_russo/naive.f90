	subroutine Multiply (A, B, C, M)
	
	! Parameters
	integer M
	double precision A(M,M)
	double precision B(M,M)
	double precision C(M,M)
	double precision Ctemp
	
	! Local variables
	integer i
	integer j
	integer k
	
	do i=1,M
		do j=1,M
			Ctemp = C(j,i)
			do k=1,M
				Ctemp = Ctemp + A(k,i)*B(j,k)
			end do
			C(j,i) = Ctemp
		end do
	end do
	
	end
