#ifndef MY_QUEUE_PSYCHO
#define MY_QUEUE_PSYCHO

#include <stdio.h>
#include <stdlib.h>
#include "node.cpp"

template <typename data_t>
class queue {
	private:
		typedef node<data_t> Node;

		Node *begin;
		Node *end;
		unsigned int size;

	public:
		queue() :
			begin(NULL),
			end(NULL),
			size(0)
		{ }

		inline bool is_empty(){
			return size==0;
		}

		inline data_t pop(){
			if( not this->is_empty() ){
				data_t ret = end->data();
				Node *n=end->next();
				delete end;
				end=n;
				--size;
				return ret;
			}else{
				printf("cabou lista\n");
				exit(-1);
			}
		}

		inline void push( const data_t &in ){
			Node *n( new Node(in) );
			if( not this->is_empty() ){
				begin->next(n);
			}else{
				end=n;
			}
			begin=n;
			++size;
		}

		~queue(){
			Node *aux=end;
			while( aux ){
				end=aux->next();
				delete aux;
				aux=end;
			}
		}
};

#endif // ifdef MY_QUEUE_PSYCHO

