#!/bin/bash

FILE="$1"

X_INI="2240.415863"

X_END="2420.767894"

Y_INI=
Y_END=

gen_ret(){
	local SCALE=10
	X1=$1
	X2=$3
	Y1=$2
	Y2=$4

	A=$( bc<<<"scale=${SCALE};($Y1-$Y2)/($X1-$X2)" )
	B=$( bc<<<"scale=${SCALE};-($A*$X1)+($Y1)" )

	for X in $( seq "${X_INI}" "${X_END}" | tr ',' '.' ) ; do
#		echo "scale=${SCALE};($A*$X)+$B"
		echo "$X $( bc<<<"scale=${SCALE};($A*$X)+$B" )"
	done
#	echo "scale=${SCALE};($Y1-$Y2)/($X1-$X2)"
#	echo "scale=${SCALE};-($A*$X1)+($Y1)"
#	echo "Y=${A}*X+$B"
}

flt_test(){
	if [ "$( bc <<<"$*" )" = 1 ] ; then
		return 0
	else
		return 1
	fi
}

while read X Y ; do
	if flt_test "${X} < ${X_INI}" || flt_test "${X_END} < ${X}" ;then
		if [ "${Y_INI}" ] && [ ! "${Y_END}" ] && flt_test "${X_END} < ${X}" ; then
			Y_END="${Y}"
			gen_ret "${X_INI}" "${Y_INI}" "${X_END}" "${Y_END}"
		else
			echo "${X} ${Y}"
		fi
	elif flt_test "${X_INI} < ${X}" && [ ! "${Y_INI}" ] ; then
		Y_INI="${Y}"
	fi
done < <( sed -e 's/\r//g' "${FILE}" )
