#!/bin/bash
#
# browser feito somente em bash
# se quiser mesmo somente em bash, descomente a linha do while
# e comente o comando cat
#
# vou deixar o cat pra ele identar melhor paginas somente texto
#
# Parametros do bashwser:
#
# $1 URL
# $2 se existe chamamos o lynx para ler a pagina 
#
# ex:ex./bashwser www.google.com.br/search?q=funcoeszz 
#funcoeszz./bashwser http://www.lcp.coppe.ufrj.br/~thobias/scr/bashwser
#bashwser.b/bbashwser www.linuxtoday.com  x
#
# PS: eu sei que nao serve pra nada. 
#     ops! serve sim, de exemplo pra voce fazer um cliente servidor 
#     usando shell
#
#shellexemplo, em uma maquina vc roda o comando
#comando$ nc -l -p 1025
#1025em uma outra maquina rode estes comandos
#comandos$ exec 3<>/dev/tcp/maquina/1025
#1025$ echo bash rlz >&3
#rlz$ echo mais bash rlz >&3
#rlz$ exec 3<&-
#
# Last update: Qua Abr  9 14:53:00 BRT 2003
#
# Thobias Salazar Trevisan <thobias at cos.ufrj.br> 
#

#[ "$1" ] || { echo "Usage: $0 <URL> [qq coisa]"; exit; }


clear
echo Begin > testlog

while [ '1' = '1' ] ; do
	(
	exec 3<>/dev/tcp/"mirror.internode.on.net"/80
	echo send GET
	echo -e "GET /pub/mozilla//firefox/releases/3.0/linux-i686/pt-BR/firefox-3.0.tar.bz2 HTTP/1.0\r\n\r\n" >&3
	cat 0<&3 > 'firefox-3.0.tar.gz' &
	sleep 1s
	echo Term GET >> testlog
	killall cat
	sleep 10s
	exec 3<&-
	)
done


