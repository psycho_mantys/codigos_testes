#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    long wait;
	if(argc>1)
	{
	    wait = atol(argv[1]);
	    wait *= 1000;
	    usleep(wait);
	}
	else
	{
	    printf("Usage:\n\tusleep <milliseconds>\n");
	}	
}
