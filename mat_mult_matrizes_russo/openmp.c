// A, B and C are all (NxN) matrices

extern "C"
{
void Multiply ( double *A, double *B, double *C, int *M )
{	
	int N = *M;

	// Perform the matrix multiplication
	#pragma omp parallel for schedule(dynamic)
	for ( int i=0; i<N; i++ )
	{
		for ( int j=0; j<N; j++ )
		{
			C[i*N+j] = 0.0;
			for ( int k=0; k<N; k++ )
				C[i*N+j] += A[i*N+k] * B[k*N+j];
		}
	}
}
}
