/*
 * =====================================================================================
 *
 *       Filename:  144.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  22-10-2010 16:28:54
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include <iostream>
#include <vector>
#include <string>
#include <deque>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <functional>

using namespace std;

struct est
{
	int id;
	int val;
	est( int eid ):
		id(eid),
		val(0)
	{
//		cout << id << endl ;
	}
};

void debug(int num_est, int moedas, int moedas_refil, deque<est> &estudantes);

int main(){
	int qtd, k ;

	cin >> qtd >> k ;
	while( qtd && k ){
		deque<est> estudantes;
		int moedas=1;
		int moedas_refil=1;

		for(int index=0 ; index<qtd ; ++index ){
			estudantes.push_back( est(index+1) );
		}

		while( estudantes.size() ){
			if( moedas<=0 ){
				if( moedas_refil>=k ){
					moedas=1;
					moedas_refil=1;
				}else{
					moedas=moedas_refil+1;
					moedas_refil++;
				}
			}

//			debug(qtd,moedas,moedas_refil,estudantes);

			est atual=estudantes.front();
			estudantes.pop_front();
			atual.val+=moedas;

//			moedas=atual.val-40;
			if( atual.val>=40 ){
				moedas=atual.val-40;
				cout << setw(3) << atual.id;
			}else{
				moedas=0;
				estudantes.push_back(atual);
			}

		}
		cout << endl ;

		cin >> qtd >> k ;
	}

	return 0;
}



int gid=0;

bool test( const est &in ){
	if( in.id==gid ){
		return true;
	}
	return false;
}

void debug(int num_est, int moedas, int moedas_refil, deque<est> &estudantes){
	clog << "Moedas: " << moedas << " " << "Moedas refil: " << moedas_refil << endl;
	for(int index=0 ; index<num_est ; ++index){
		gid=index+1;
		deque<est>::iterator temp=find_if(estudantes.begin(),estudantes.end(),test);
		if( temp!=estudantes.end() )
			clog << "|  Id: " << setw(3) << (*temp).id << " val: " << setw(3) << (*temp).val ;
	}
	clog << endl << "Fila em ordem normal:"<< endl ;
	for(unsigned int index=0 ; index<estudantes.size() ; ++index){
		clog << "|  Id: " << setw(3) << estudantes.at(index).id << " val: " << setw(3) << estudantes.at(index).val ;
	}
	clog << endl ;
	clog << endl ;
}


