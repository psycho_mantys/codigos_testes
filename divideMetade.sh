#!/bin/bash

[ ! "$1" ] && exit 1

TEMP_LOG="$( mktemp )"

METADE=$( echo "$(du -b $1 | cut -f 1 ) / 2" | bc )

dd bs=1 count="${METADE}" if="$1" of="${TEMP_LOG}"

cat "${TEMP_LOG}" > "$1"

rm "${TEMP_LOG}"
