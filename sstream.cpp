
#include	<sstream>
#include	<string>
#include	<iostream>

using namespace std;

int main(){
	stringstream ss;

	ss << 1 ;
	ss << 2 ;
	ss << 3 ;
	ss << 4 ;
	cout << ss.str() << "\n" ;

	ss.sync();
	ss.clear();
	ss.flush();
	ss.exceptions();
	ss << 1 ;
	ss << 2 ;
	ss << 3 ;
	ss << 4 ;
	int x;
	ss >> x;
	cout << ss.str() << " " << x<< "\n" ;
	return 0;
}

