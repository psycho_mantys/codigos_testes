#include <time.h>
#include <stdio.h>
#include <string.h>

time_t t;

int main(){
	/* 26 e o tamanho do char para caber a string formatada */
	char date[26];

	/* Pega o tempo */
	t=time(NULL); /* ou time(&t); */
	
	/* Copia para date o tempo formatado */
	strcpy( date , ctime( &t ) );
	
	printf("date: %s", date );
	printf("Tamanho: %d\n", strlen(date) );
	return 0;
}
