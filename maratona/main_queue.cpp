#include <iostream>
#include "queue.cpp"

using namespace std;

int main(){
	queue<int> s;
	s.push(1);
	s.push(2);
	s.push(3);
	s.push(4);
	s.push(5);
	s.push(6);
	s.push(7);
	
	while( not s.is_empty() ){
		cout << s.pop() << endl ;
	}

	s.push(1);
	s.push(2);
	s.push(3);
	while( not s.is_empty() ){
		cout << s.pop() << endl ;
	}

	s.push(1);
	s.push(2);
	while( not s.is_empty() ){
		cout << s.pop() << endl ;
	}
	s.push(3);

//	cout << s.pop() << endl ;
//	cout << s.pop() << endl ;

	return 0;
}

