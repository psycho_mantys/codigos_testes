/* // Gerador de sistemas de equao?=es */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int main (void)
{
/*	// forma: K.X = R */
	int numEq, numIt, i, k, type;
/*	//int var = 'nl'; */
	
   FILE *fp;
	char namefile[100];
	double *alphaS;
	clock_t start, end;

/*	// Variáveis utilizadas*/
   double tol, maior ;
	double *R, *K;
	double zero = 0.0;
	
	printf("\nGERADOR DE SISTEMAS DE EQUACOES \n");
	printf("\nNumero de equacoes  : ");   scanf("%d",&numEq);
	printf("Numero de iteracoes   : ");   scanf("%d",&numIt);
	printf("Tolerancia            : ");   scanf("%lf",&tol);
   printf("Formato de escrita    : ");   scanf("%d",&type);

	alphaS = (double *)malloc(numEq*sizeof(double));
	
	start = clock();

	sprintf(namefile,"%s_%d.dat","sistema",numEq);
	
   fp = fopen(namefile,"wb");

   printf("\a \n\nPROCESSANDO ...\n");

/*   // ALOCAÇÃO DOS VETORES E MATRIZES ___________________________________________________________________________*/
   R = (double *)malloc(numEq*sizeof(double));
   for(i=0;i<numEq;i++)
      R[i] = 0;

/*   //X = (double *)malloc(numEq*sizeof(double));  ... naum está gerando a solução*/

   K = (double *)malloc(numEq*sizeof(double ));

   srand(100);

   fprintf(fp,"%d \n", numEq);
   fprintf(fp,"\n");

/*   // GERAÇÃO DAS MATRIZES E ELEMENTOS __________________________________________________________________________*/
   for(i=0;i<numEq;i++)
      R[i] = rand()%100;

   for(i=0;i<numEq;i++)
   {
/*      // ------------------------------*/
      maior  = -10^10;
      for(k=0;k<numEq;k++)
      {
         K[k] = rand()%100;
         if(K[k] > maior)
            maior = K[k];
      }
      K[i] = 1000000*maior;
      
      for(k=0;k<numEq;k++)
         fprintf(fp,"%.3f  ",K[k]);		

      fprintf(fp,"\n");
     
/*     printf("%d\n",i);*/
     

   }
   fprintf(fp,"\n");

	/* / IMPRESSÃO NO FORMATO BINÁRIO  _________________________________________________________________________________*/
	if(type == 1)
   {
      #define ASCII
      fwrite(&numEq,sizeof(int),1,fp);
/*      //fprintf(fp,"\n"); */

/*      // Matrix K */
      for(i=0;i<numEq;i++)
      {
/*         //for(j=0;j<numEq;j++)
            //fwrite(&K[i][j],sizeof(double),1,fp); */
      }

/*      // Vetor Resultado */
      for(i=0;i<numEq;i++)
         fwrite(&R[i],sizeof(double),1,fp);

/*      // Vetor de Chute inicial */
      for(i=0;i<numEq;i++)
         fwrite(&zero,sizeof(double),1,fp);

      fwrite(&numIt,sizeof(int),1,fp);
      fwrite(&tol,sizeof(double),1,fp);

      /*for(i=0;i<numEq;i++)
         fwrite(&X[i],sizeof(double),1,fp);*/

      end = clock();
/*      // _______________________________________________________________________________________________________ END ARQ.BIN */
   }


/*	// IMPRESSÃO NO FORMATO DECIMAL ______________________________________________________________________________________ */
   else
   {


/*      // Impressão do vetor constante ou Resultado */
      for(i=0;i<numEq;i++)
      {
         fprintf(fp,"%.2f\n",R[i]);
      }
      fprintf(fp, "\n");

/*      // Impressão do Chute inicial*/
      for(i=0;i<numEq;i++)
      {
         fprintf(fp,"%.2f\n",zero);
      }
      fprintf(fp, "\n");

/*      // Número de Iterações e tolerância*/
      fprintf(fp,"%d \n", numIt);
      fprintf(fp,"%f\n", tol);

/*      //fprintf(fp, "\n"); */

/*      //fprintf(fp," ________________________ SOLUCAO .\n");*/

      /*for(i=0;i<numEq;i++)
         fprintf(fp,"%lf\n",X[i]);*/

      end = clock();
/*	// _______________________________________________________________________________________________________ END ARQ.DEC */
}
	printf("\a \n ...ARQUIVO PRONTO  TIME = %f\n\n",((double)(end - start))/CLOCKS_PER_SEC);

   printf("\a\a \n\n antes de libear  ...");  
/*   //Liberando a memória*/
/*   //for(i=0;i<numEq;i++)*/
/*     // free(K[i]); */

   free(K);
   free(R);

   printf("\a\a \n\n END memória liberada ...");


return 0;
}
 
