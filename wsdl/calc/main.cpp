/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  A simple gsoap client sample.
 *
 *        Version:  1.0
 *        Created:  19-08-2009 21:07:59
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Psycho Mantys (P.M.), psycho.mantys(.AT,)gmail dot com
 *        Company:  LCCV
 *
 * =====================================================================================
 */

#include "calc.nsmap"
#include "soapcalcProxy.h"
#include <iostream>

using namespace std;

int main(){
	calc math_ws ;
	double res = 0;

	if( math_ws.ns2__mul( 1.0 , 2.0 , res ) )
		cerr << "WS invocation error." << endl ;

	cout << "mul:" << res << endl ;
	return 0;
}

