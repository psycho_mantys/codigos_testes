#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "mpi.h"

#define NUMMAXTAG  100
#define NUMMAXTAG2  200
#define ASCII

struct time{
	
	double t_aloc;
	double t_leit;
	double t_proc;
	double t_comun;
	double t_total;
} Time;


/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Funcao GaussJacobiMaster
///////////////////////////////////////////////////////////////////////////////////////////////// */
int GaussJacobiMaster(int n,double *x0, int nmaxite, double tol, double *x, int *nite,int *nlpp)
{
	double norma;
	int flag;
	double taux;
	int i,k;
	int inicio;
	int size;
	int tag;
	MPI_Status status;

	MPI_Comm_size(MPI_COMM_WORLD,&size);

/* 	// Inicializacao das variaveis*/
	flag = 2;
	*nite=0;
	norma=tol+1;

/*	// Envia flags para todos os processos avisando-os para iniciarem os trabalhos */
	taux = MPI_Wtime();
	for (k=1;k<size;k++)
		MPI_Send(&flag, 1, MPI_INT,k, NUMMAXTAG, MPI_COMM_WORLD);
	
/*	// Incrementa 1 a variavel que sera utilizada como tag nas mensagens seguintes */
	tag = NUMMAXTAG+1;
	  

/*	// Inicio do processo iterativo */
/*	//while (*nite<=nmaxite && norma>tol) */
	while(*nite<=nmaxite)
	{
/*		// incrementa numero de iteracoes*/
		(*nite)++;

/*		//envia x0 para os escravos*/
		MPI_Bcast(x0, n, MPI_DOUBLE,0, MPI_COMM_WORLD); 

/*		// Inicializa variaveis*/
		inicio=0;

		for (k=1;k<size;k++)
		{  
			MPI_Recv(&x[inicio], nlpp[k], MPI_DOUBLE, k, tag, MPI_COMM_WORLD,&status);
			inicio+=nlpp[k];
		}
		
		tag++;

/*		//teste de convergencia*/
		norma=0.0;
		taux =  MPI_Wtime();
		for (i=0;i<n;i++)
		{
			norma=norma+ (x[i]-x0[i])*(x[i]-x0[i]);
			x0[i]=x[i];
		}
		norma=sqrt(norma);
		
/*		// Caso tenha chegado ao final (convergido) seta variavel flag para interromper trabalho
		// dos escravos */
/*	//	if (*nite<=nmaxite && norma>tol) {*/
		if (*nite<=nmaxite) {
			flag = 2;
		} else {
			flag = 3;
		}

/*		// Envia flag para os escravos comunicando se eles devem continuar ou interromper o trabalho*/
		for (k=1;k<size;k++)
			MPI_Send(&flag, 1, MPI_INT,k, tag, MPI_COMM_WORLD);

		tag++;
	}

	

	return 1;
}

/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Fim da Funcao GaussJacobiMaster
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
// Funcao GaussJacobiSlave
///////////////////////////////////////////////////////////////////////////////////////////////// */
void GaussJacobiSlave(int n, double **A, double *b, double *x0, int *nlpp,int nmaxite, double tol,int NumThreads )
{
	int    flag;
	double s;
	int tag;
	int size;
	int rank;
	int i,j;
	double *x;
	MPI_Status status;
	int aux = 0, aux2 = 0;

	
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	x = (double*)malloc(nlpp[rank]*sizeof(double));

	for (i=1;i<rank;i++)   
		aux+=nlpp[i];

/*	// Recebe flag para saber se o slave deve continuar trabalhando ou nao */
	MPI_Recv(&flag, 1, MPI_INT,0,NUMMAXTAG,MPI_COMM_WORLD,&status);
	
/*	// Incrementa tag */
	tag =NUMMAXTAG+1;
	
	#pragma omp parallel num_threads(NumThreads)	
	{
		while(flag==2)
		{

			#pragma omp single
/*			// Recebimento dos dados de x0 vindo do master */
			MPI_Bcast(x0, n, MPI_DOUBLE,0, MPI_COMM_WORLD); 

		
			/* /#pragma omp parallel for private (s,j,aux2) num_threads(NumThreads)	*/
			#pragma omp for private(s,j,aux2)
			/* / Processamento da parte do sistema correspondente ao escravo em questao */
			for (i=0;i<nlpp[rank];i++)   
			{	
				aux2 =i+aux;
				s=0;
				for (j=0;j<aux2;j++)
					s += (A[i][j]*x0[j]);				
				++j;
				for(;j<n;++j)
					s += (A[i][j]*x0[j]);
				x[i]=(b[i] - s)/A[i][aux2];
			} 
			
			#pragma omp single
			{
			/* / Envia o vetor x inteiro deste processo para o mestre */
			MPI_Send(x, nlpp[rank], MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
			tag++;

			/* / Recebe flag para saber se o slave deve continuar trabalhando ou nao */
			MPI_Recv(&flag, 1, MPI_INT,0,tag,MPI_COMM_WORLD,&status);
			tag++;
			}	

	
		}
	}

}

/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Fim da Funcao GaussJacobiSlave
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
// Funcao ReadDataFile
///////////////////////////////////////////////////////////////////////////////////////////////// */
int readDataFile(FILE *fp, double ***A, double **b, double **x0, double **x, int *nmaxite, double *tol) {

	int n =3, i, j;
	
	/*Pega o tamanho da matriz*/
	fscanf(fp,"%d",&n);

/*	//inicia a contagem do tempo de aloca��o*/
	Time.t_aloc = MPI_Wtime() ;

	/*Aloca a matriz e os vetores com base no n*/
	*A = calloc(n, sizeof(double *));
	for (i = 0; i < n; i++)
		(*A)[i] = calloc(n, sizeof(double));
	*b  = calloc(n, sizeof(double));
	*x0 = calloc(n, sizeof(double));
	*x = calloc(n, sizeof(double));

/*	//finaliza a contagem do tempo de aloca��o*/
	Time.t_aloc = MPI_Wtime() - Time.t_aloc ;

/*	//inicia a contagem do tempo de leitura do arquivo de entrada*/
	Time.t_leit = MPI_Wtime() ;

	/*Atribui os valores lidos do arquivo � matriz e aos vetores*/
	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			fscanf(fp, "%lf", &((*A)[i][j]));
	for(i = 0; i < n; i++)
		fscanf(fp, "%lf", &((*b)[i]));
	for(i = 0; i < n; i++)
		fscanf(fp, "%lf", &((*x0)[i]));
	fscanf(fp, "%d", nmaxite);
	fscanf(fp, "%lf", tol);

/*	//finaliza a contagem do tempo de aloca��o*/
	Time.t_leit = MPI_Wtime() - Time.t_leit ;

	return n;

}


/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Fim da Funcao ReadDataFile
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
// Funcao main
///////////////////////////////////////////////////////////////////////////////////////////////// */
int main(int argc,char *argv[])
{

	int    n;                    /* / Numero de Equacoes*/
	double **A;                  /* / Matriz dos Coeficientes*/
	double *b;                   /* / Vetor b*/
	double *x0;                  /* / Vetor x0*/
	double tol;                  /* / Tolerancia admitida*/
	int    nmaxite;              /* / Numero maximo de iteracoes*/
	double *x;                   /* / Vetor solucao*/
	int    i,j;                /* / Indexadores da matriz*/
	int    nite;                 /* / Numero total de iteracoes*/
	FILE   *fpin;                /* / Ponteiro para o arquivo de dados*/
	int    size;	             /* / Numero de processos*/
	int    rank;		         /* / Identifica�ao do processo*/
	char   processor_name[500];  /* / Nome do computador que esta com o processo*/
	int    namelen;              /* / Variavel auxiliar*/
	int    *nlpp;                /* / Vetor contendo o numero de linha por processo (eh do tamanho do numero de processos)*/
	int    nlp;                  /* / Variavel auxiliar para contabilizar o numero de linha por processo*/
	int    resto;                /* / Variavel auxiliar para contar o resto da divisao do numero de linhas pelo num. de processos*/
	int    inicio;               /* / Variavel auxiliar*/
	int    tag;			         /* / rotulo de um dado (mensagem)*/
	MPI_Status status;           /* / Variavel auxiliar para retorno do MPI */
	int  NumThreads;
	
	
/*	// Inicializa o MPI e captura informacoes globais sobre os processos
	// Sera executado em todos os processos */
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Get_processor_name(processor_name,&namelen);

	NumThreads = atoi(argv[2]);

/*	//inicia a contagem do tempo total de execu��o*/
	Time.t_total = MPI_Wtime();
	
	if (rank==0) {

/*		// Abre arquivo de entrada */
		fpin = fopen(argv[1],"r");

		if (fpin==NULL){
			fprintf(stdout,"Erro ao tentar abrir o arquivo de dados\n");
			fflush(stdout);
		}


/*		// Leitura do arquivo de dados */
		n = readDataFile(fpin, &A, &b, &x0, &x, &nmaxite, &tol);


/*		// Calcula numero de linhas por processador*/
		nlp     = (int)n/(size-1);

/*		//aloca o vetor que contem o numero de linhas por processador*/
		nlpp  = (int*)malloc(size*sizeof(int));

		nlpp[0] = 0;
		for(i=1;i<size;i++)
			nlpp[i] = nlp;


/*		// Calcula resto da divisao */
		resto = (int)n%(size-1);


/*		// Incrementa mais um processo para cada processador ate o resto ser igual a zero*/
		i = 1;
		if (resto!=0){
			while (resto>0){
				nlpp[i]++;
				resto--;
				i++;
			}
		}

/*		// Inicializa variaveis */
		inicio = 0;



/*		//inicia a contagem do tempo de envio de dados iniciais*/
		Time.t_comun = MPI_Wtime() ;

/*		// Envia numero de equacoes do sistema para os processos escravos */
		MPI_Bcast(&n,1, MPI_INT,0, MPI_COMM_WORLD);

/*		// Envia o vetor com o  numero de equacoes por processo */
		MPI_Bcast(nlpp, size, MPI_INT,0, MPI_COMM_WORLD);

/*		// Enviar dados para todos os processos escravos*/
		for (i=1;i<size;i++){

			tag=2;
			MPI_Send(&(b[inicio]), nlpp[i], MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
			tag++;
			for (j=inicio; j<(inicio+nlpp[i]); j++){
				MPI_Send(A[j], n, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
				tag++;
			}
			inicio += nlpp[i];
		}

/*		//finaliza a contagem do tempo de envio de dados iniciais*/
		Time.t_comun = MPI_Wtime() - Time.t_comun ;

/*		//inicia a contagem do tempo de processamento*/
		Time.t_proc = MPI_Wtime() ;

/*		// Chama a fun�ao que gerencia os processos */
		GaussJacobiMaster(n,x0,nmaxite,tol,x, &nite,nlpp);
		
/*		//finaliza a contagem do tempo de processamento */
		Time.t_proc = MPI_Wtime() - Time.t_proc ;
	
/*		// Impressao da solucao do sistema */
		fprintf(stdout,"\n\nA solucao do encontrada apos %d iteracoes foi:\n",nite);
		fprintf(stdout,"X[%d] = %f\n",0,x[0]);
		fprintf(stdout,"X[%d] = %f\n",n-1,x[n-1]);
		fflush(stdout);
	
/*		// Libera memoria alocada para a matriz dos coeficientes, a liberacao dos outras variaveis que foram alocadas
		// dinamicamente sao feitas na parte final do codigo por serem comuns ao master e aos slaves */
		for(i=0;i<n;i++)
			if (A[i]!=NULL) free(A[i]);
		if (A!=NULL) free(A);


/*		// Fecha os arquivos de entrada e saida: Como foram abertos pelo mestre (e no mestre) devem ser fechados 
		// pelo mestre */
		fclose(fpin);
/*		//fclose(fpout);*/

	} else {  /* / INICIO DO PROGRAMA SLAVE */
		
		
/*		//aloca o vetor que contem o numero de linhas por processador*/
		nlpp  = (int*)malloc(size*sizeof(int));

		
/*		// Recebe dados n e nlpp do processos mestre */
		MPI_Bcast(&n,1, MPI_INT,0, MPI_COMM_WORLD);
		MPI_Bcast(nlpp, size, MPI_INT,0, MPI_COMM_WORLD);


/*		// Alocacao dinamica da matriz A e dos vetores b,xo ---> Observar que as dimensoes (tamanhos) da matriz e do vetor sao
		// adequadas ao necessario para cada slave gerando uma economia no uso da memoria dos processos escravos */
		A = (double **)malloc(nlpp[rank]*sizeof(double *));
		for (i=0;i<nlpp[rank];i++)
			A[i] = (double *)malloc(n*sizeof(double));
		b  = (double *)calloc(nlpp[rank],sizeof(double));
		x0 = (double *)calloc(n,sizeof(double));
		x  = (double *)calloc(nlpp[rank],sizeof(double));
		
		tag=2;
		MPI_Recv(b, nlpp[rank], MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);
		tag++;
		for(i=0;i<nlpp[rank];i++){
			MPI_Recv(A[i],n, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);
			tag++;
		}
		
	
/*		// Chama a fun��o que realiza as itera��es nos processos escravos quando forem acionadas pelo master*/
		GaussJacobiSlave(n,A,b,x0,nlpp,nmaxite,tol,NumThreads);

/*		// Libera memoria alocada para a matriz A e os demais vetores alocados no slave sao liberado na parte final
		// do programa por serem liberacoes comuns ao mestre e aos escravos */
		for(i=0;i<nlpp[rank];i++)
			if (A[i]!=NULL) free(A[i]);
		if (A!=NULL) free(A);

	} /* /finaliza o else */

	/* / Libera memoria alocada de variaveis comuns a todos os processos (master e slaves) */
	if (nlpp!=NULL) free(nlpp);
	if (b!=NULL) free(b);
	if (x0!=NULL) free(x0);
	if (x!=NULL)free(x);

	/* /finaliza a contagem do tempo total de execu��o*/
	Time.t_total = MPI_Wtime() -Time.t_total;

	/* /impress�o dos tempos*/
	if(rank==0){
	printf(" \n\nFIM DA EXECUCAO : ");
	printf( " \n\nNome do Executavel:%s",argv[0]);
	printf( " \nNumero de Processos:%d",size);
	printf( " \nNumero de Threads:%d",NumThreads);
	printf("\nArquivo de Entrada:%s",argv[1]);
	printf("\n\nTempo de Alocacao:%f",Time.t_aloc);
	printf("\nTempo de Leitura:%f",Time.t_leit);
	printf("\nTempo de Processamento:%f",Time.t_proc);
	printf("\nTempo total:%f\n\n",Time.t_total);	
	}
	

/*	// Finaliza o MPI em todos os processos*/
	MPI_Finalize();

	return 0;
}

/* /////////////////////////////////////////////////////////////////////////////////////////////////
// Fim da Funcao main e do Programa
///////////////////////////////////////////////////////////////////////////////////////////////// */
