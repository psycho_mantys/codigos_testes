#!/bin/bash

rsync -avz --filter='- **/bin*'  --filter='- **/dev*' --filter='- **/sys*' --filter='- **/proc*' --filter='- **/dev*' --filter='- **/boot*' --filter='- **/var*' -e "ssh -i /etc/cron.daily/thishost-rsync-key" / psycho@192.168.1.252:~/sync/
