/* Gerador de sistemas de equao?=es */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int main (void)
{
	/* / forma: K.X = R */
	int numEq, numIt, i, j, k, type;
	/* /int var = 'nl'; */
	FILE *fp;
	char namefile[100];
	double *alphaS;
	clock_t start, end = 0;

	double tol, maior, acm1, acm2, alpha, beta, s, s0, s1;
	double *R, *X, **K;
	double zero = 0.0;
	
	printf("\nGERADOR DE SISTEMAS DE EQUACOES \n");
	printf("\nNumero de equacoes  : ");   scanf("%d",&numEq);
	printf("Numero de iteracoes   : ");   scanf("%d",&numIt);
	printf("Tolerancia            : ");   scanf("%lf",&tol);
    printf("Formato de escrita    : ");   scanf("%d",&type);

	alphaS = (double *)malloc(numEq*sizeof(double));
	
	start = clock();

	sprintf(namefile,"%s_%d.dat","sistema",numEq);
	
	fp = fopen(namefile,"wb");
	
	printf("\a \n\nPROCESSANDO ...\n");
		
    /* / ALOCAÇÃO DOS VETORES E MATRIZES ___________________________________________________________________________ */
	R = (double *)malloc(numEq*sizeof(double));
	for(i=0;i<numEq;i++)
		R[i] = 0;

	X = (double *)malloc(numEq*sizeof(double));

	K = (double **)malloc(numEq*sizeof(double *));
	for(i=0;i<numEq;i++)
		K[i] = (double *)malloc(numEq*sizeof(double));

	srand(100);

/*	// GERAÇÃO DAS MATRIZES E ELEMENTOS __________________________________________________________________________*/
	for(i=0;i<numEq;i++)
		X[i] = rand()%100;

	for(i=0;i<numEq;i++)
	{
		for(j=0;j<numEq;j++)
			K[i][j] = rand()%100;
	}

	for(i=0;i<numEq;i++)
	{
		maior = K[i][i];
		for(j=0;j<numEq;j++)
		{
			if(K[i][j] > maior)
			maior = K[i][j];
		}	
		K[i][i] = 100*maior*1000;
	}

	for(i=0;i<numEq;i++)
	{
		for(j=0;j<numEq;j++)
			R[i] = R[i] + K[i][j]*X[j];
	}

/*	// TESTE DE CONVERGÊNCIA: MÉTODO DAS COLUNAS E DAS LINHAS */
	for(i =0;i<numEq;i++)
	{
		acm1 = 0.0;
		acm2 = 0.0;
		for(j=0;j<numEq;j++)
		{
			acm1 = acm1 + abs(K[i][j]);
			acm2 = acm2 + abs(K[j][i]);
		}

		alpha = (acm1 - K[i][i]) / K[i][i];  /* /Critério das linhas */
		beta  = (acm2 - K[i][i]) / K[i][i];  /* /Critério das colunas */

		if ( (alpha > 1) || (beta > 1) )
		{
			printf("\n\aERRO NA COVERGENCIA! alpha = %f   beta = %f\n", alpha, beta);
			break;
		}
	}
			
	s=0;
	for (j=0;j<numEq;j++){
		s=s+abs(K[0][j]);
	}
	alphaS[0]=s/abs(K[0][0]);

	for (k=0;k<numEq;k++)
	{
		s0=0;
		for (j=0;j<k;j++)
			s0 = s0 + abs(K[k][j])*alphaS[j];
		
		s1=0;
		
		for (j=k+1;j<numEq;j++)
			s1= s1 + abs(K[k][j]);

		alphaS[k]=(s0+s1)/abs(K[k][k]);
		
		if (alphaS[k] > 1){
			printf("\n\aERRO 143\n");
			break;
		}

	}



/*	// IMPRESSÃO NO FORMATO BINÁRIO  _________________________________________________________________________________*/
	if(type == 1){
	#define ASCII
	fwrite(&numEq,sizeof(int),1,fp);
/*	//fprintf(fp,"\n"); */

/*	// Matrix K*/
	for(i=0;i<numEq;i++)
	{
	  for(j=0;j<numEq;j++)
		fwrite(&K[i][j],sizeof(double),1,fp);
			
/*	  //fprintf(fp, "\n");*/
	}
/*	  //fprintf(fp, "\n");*/
 
/*       // Vetor Resultado */
	for(i=0;i<numEq;i++)
	  fwrite(&R[i],sizeof(double),1,fp);

/*	//fprintf(fp, "\n"); */
	
/*	// Vetor de Chute inicial */
	for(i=0;i<numEq;i++)
	  fwrite(&zero,sizeof(double),1,fp);

/*	//fprintf(fp, "\n"); */

	fwrite(&numIt,sizeof(int),1,fp);
	fwrite(&tol,sizeof(double),1,fp);


/*	//fprintf(fp, "\n"); */

/*	//fprintf(fp," ________________________ SOLUCAO .\n"); */

	for(i=0;i<numEq;i++)
	  fwrite(&X[i],sizeof(double),1,fp);
/*	// _______________________________________________________________________________________________________ END ARQ.BIN */
}

/*	// IMPRESSÃO NO FORMATO DECIMAL ______________________________________________________________________________________ */
	else{
	fprintf(fp,"%d \n", numEq);
	fprintf(fp,"\n");

/*	// Matriz K */
	for(i=0;i<numEq;i++)
	{
	   for(j=0;j<numEq;j++)
		fprintf(fp,"%.3f  ",K[i][j]);		
		
	   fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

/*	// Impressão do vetor constante ou Resultado */
	for(i=0;i<numEq;i++)
	{
	  fprintf(fp,"%.2f\n",R[i]);
	}
	fprintf(fp, "\n");
	
/*	// Impressão do Chute inicial */
	for(i=0;i<numEq;i++)
	{
	  fprintf(fp,"%.2f\n",zero);
	}
	fprintf(fp, "\n");
	
/*	// Número de Iterações e tolerância*/
	fprintf(fp,"%d \n", numIt);
	fprintf(fp,"%f\n", tol);

	fprintf(fp, "\n");

	fprintf(fp," ________________________ SOLUCAO .\n");

	for(i=0;i<numEq;i++)
	   fprintf(fp,"%f\n",X[i]);

	end = clock();
/*	// _______________________________________________________________________________________________________ END ARQ.DEC */
}
	printf("\a \n ...ARQUIVO PRONTO  TIME = %f\n\n",((double)(end - start))/CLOCKS_PER_SEC);

return 0;
}
 
